#include "model.h"

extern "C" {
#define TINYOBJ_LOADER_C_IMPLEMENTATION
#include "tinyobj_loader_c/tinyobj_loader_c.h"
};

// todo: get rid of std:: dependency
#include <vector>
#include <string>
#include <sstream>
using namespace std;

static bool load_obj(
	const char* obj,
	size_t obj_len,
	vector<float>& out_vertices,
	vector<float>& out_normals,
	vector<float>& out_texcoords)
{
	out_vertices.clear(); out_normals.clear(); out_texcoords.clear();

	size_t num_shapes, num_materials;
	tinyobj_attrib_t attrib; DEFER(tinyobj_attrib_free(&attrib));
	tinyobj_shape_t* shapes = NULL; DEFER(tinyobj_shapes_free(shapes, num_shapes));
	tinyobj_material_t* materials = NULL; DEFER(tinyobj_materials_free(materials, num_materials));
	if(tinyobj_parse_obj(
		&attrib,
		&shapes, &num_shapes,
		&materials, &num_materials,
		obj, obj_len,
		TINYOBJ_FLAG_TRIANGULATE) != TINYOBJ_SUCCESS) return false;

	out_vertices.reserve(3*attrib.num_faces);
	out_normals.reserve(3*attrib.num_faces);
	out_texcoords.reserve(2*attrib.num_faces);
	for(size_t i = 0; i < attrib.num_faces; ++i) {
		uint32_t vidx = attrib.faces[i].v_idx*3;
		out_vertices.push_back(attrib.vertices[vidx + 0]);
		out_vertices.push_back(attrib.vertices[vidx + 1]);
		out_vertices.push_back(attrib.vertices[vidx + 2]);
		uint32_t nidx = attrib.faces[i].vn_idx*3;
		out_normals.push_back(attrib.normals[nidx + 0]);
		out_normals.push_back(attrib.normals[nidx + 1]);
		out_normals.push_back(attrib.normals[nidx + 2]);
		uint32_t tidx = attrib.faces[i].vt_idx*2;
		out_texcoords.push_back(attrib.texcoords[tidx + 0]);
		out_texcoords.push_back(attrib.texcoords[tidx + 1]);
	}
	return true;
}

void calc_tangents_bitangents(
	const vector<float>& vertices,
	const vector<float>& normals,
	const vector<float>& texcoords,
	vector<float>& out_tangents,
	vector<float>& out_bitangents) 
{
	out_tangents.reserve(vertices.size());
	out_bitangents.reserve(vertices.size());
	lassert((vertices.size()/3)%3 == 0);
	lassert((texcoords.size()/2)%3 == 0);
	for(size_t i = 0; i < vertices.size()/3; i += 3) {
		size_t b3 = i*3, b2 = i*2;
		const vec3& v0 = *(vec3*)&vertices[b3 + 0*3];
		const vec3& v1 = *(vec3*)&vertices[b3 + 1*3];
		const vec3& v2 = *(vec3*)&vertices[b3 + 2*3];
		const vec2& uv0 = *(vec2*)&texcoords[b2 + 0*2];
		const vec2& uv1 = *(vec2*)&texcoords[b2 + 1*2];
		const vec2& uv2 = *(vec2*)&texcoords[b2 + 2*2];
		vec3 dpos1 = v1 - v0;
		vec3 dpos2 = v2 - v0;
		vec2 duv1 = uv1 - uv0;
		vec2 duv2 = uv2 - uv0;
		float r = (duv1.x*duv2.y - duv1.y*duv2.x);
		vec3 tangent, bitangent;
		if(fabs(r) < LEPSILON) {
			tangent = vec3(0.0f, 1.0f, 0.0f);
			bitangent = vec3(0.0f, 0.0f, 1.0f);
		} else {
			r = 1.0f/r;
			tangent = r*(dpos1*duv2.y - dpos2*duv1.y);
			bitangent = r*(dpos2*duv1.x - dpos1*duv2.x);
		}

		out_tangents.push_back(tangent.x);
		out_tangents.push_back(tangent.y);
		out_tangents.push_back(tangent.z);
		out_tangents.push_back(tangent.x);
		out_tangents.push_back(tangent.y);
		out_tangents.push_back(tangent.z);
		out_tangents.push_back(tangent.x);
		out_tangents.push_back(tangent.y);
		out_tangents.push_back(tangent.z);
		out_bitangents.push_back(bitangent.x);
		out_bitangents.push_back(bitangent.y);
		out_bitangents.push_back(bitangent.z);
		out_bitangents.push_back(bitangent.x);
		out_bitangents.push_back(bitangent.y);
		out_bitangents.push_back(bitangent.z);
		out_bitangents.push_back(bitangent.x);
		out_bitangents.push_back(bitangent.y);
		out_bitangents.push_back(bitangent.z);
	}
	lassert(out_tangents.size() == out_bitangents.size() && out_bitangents.size() == normals.size());

#if 0
	const vec3& normal = *(vec3*)&normals[b3 + 0*3];
	tangent = normalize(tangent - normal*dot(normal, tangent));
	if(dot(cross(normal, tangent), bitangent) < 0.0f) {
		tangent *= -1.0f;
	}
#endif
}

bool near(float f0, float f1) { return fabs(f0 - f1) < 0.01f; }
void calc_indices(
	vector<float>& flat_vertices,
	vector<float>& flat_normals,
	vector<float>& flat_texcoords,
	vector<float>& out_vertices,
	vector<float>& out_normals,
	vector<float>& out_texcoords,
	vector<uint16_t>& out_indices)
{
	out_vertices = flat_vertices;
	out_normals = flat_normals;
	out_texcoords = flat_texcoords;
	out_indices.reserve(flat_vertices.size()/3);
	for(size_t i = 0; i < flat_vertices.size()/3; ++i) {
		out_indices.push_back((uint16_t)i);
	}
}

bool model_t::load_from_obj(const char* file) {
	this->destroy();

	string obj = lload_file(file);
	obj += "\n"; // bug in tinyobj_loader_c
	size_t obj_len = obj.size();
	if(!obj.size()) return false;

	vector<float> flat_vertices;
	vector<float> flat_normals;
	vector<float> flat_texcoords;
	if(!load_obj(obj.c_str(), obj_len, flat_vertices, flat_normals, flat_texcoords)) return false;
	if(!(flat_vertices.size()%3 == 0)) return false;
	if(!(flat_normals.size()%3 == 0)) return false;
	if(!(flat_texcoords.size()%2 == 0)) return false;
	if(flat_vertices.size() != flat_normals.size() || flat_normals.size()/3 != flat_texcoords.size()/2) return false;

	vector<float> real_vertices, real_normals, real_texcoords;
	vector<uint16_t> real_indices;
	calc_indices(
		flat_vertices, flat_normals, flat_texcoords,
		real_vertices, real_normals, real_texcoords,
		real_indices
	);
	if(!(real_vertices.size()%3 == 0)) return false;
	if(!(real_normals.size()%3 == 0)) return false;
	if(!(real_texcoords.size()%2 == 0)) return false;
	if(real_vertices.size() != real_normals.size() || 
	   real_vertices.size()/3 != real_texcoords.size()/2) return false;

	vertices   = (float*)   malloc(sizeof(*real_vertices  .data())*real_vertices  .size());
	texcoords  = (float*)   malloc(sizeof(*real_texcoords .data())*real_texcoords .size());
	normals    = (float*)   malloc(sizeof(*real_normals   .data())*real_normals   .size());
	indices    = (uint16_t*)malloc(sizeof(*real_indices   .data())*real_indices   .size());

	n_indices = real_indices.size();
	n_vertices = real_vertices.size()/3;

	memcpy(vertices,   real_vertices.data(),   real_vertices  .size()*sizeof(*real_vertices  .data()));
	memcpy(texcoords,  real_texcoords.data(),  real_texcoords .size()*sizeof(*real_texcoords .data()));
	memcpy(normals,    real_normals.data(),    real_normals   .size()*sizeof(*real_normals   .data()));
	memcpy(indices,    real_indices.data(),    real_indices   .size()*sizeof(*real_indices   .data()));

	return true;
}
