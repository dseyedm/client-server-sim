#pragma once

#include "defer.h"
#include "ltime.h"
//#include "lserialize.h"

#if 0
#include "vectorial/vectorial.h"
using namespace vectorial;
typedef vec3f vec3;
typedef vec2f vec2;
typedef mat4f mat4;
#else
#include <glm/glm.hpp>
#include <glm/gtx/compatibility.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;
#endif

#define UNUSED(u) (void)(u)
#define LENGTH(a) (sizeof(a)/sizeof(*a))
#define ZEROM(s) memset(&s, 0, sizeof(s))

#include <string.h> // mem*
#include <string> // std::string (should be replaced ? )

#include <stdint.h>
typedef int8_t i8;
typedef uint8_t u8;
typedef int16_t i16;
typedef uint16_t u16;
typedef int32_t i32;
typedef uint32_t u32;
typedef int64_t i64;
typedef uint64_t u64;
typedef float f32;
typedef double f64;

#include <stdio.h>
#define CHECKPOINT fprintf(stderr, "@ function %s on line %d in %s\n", __FUNCTION__, __LINE__, __FILE__)

#ifndef NDEBUG
#define lassert(c) ((c) ? (void)0 : _lassert(#c, __FILE__, __FUNCTION__, __LINE__))
#else
#define lassert(c) if(c) { (void)(0); }
#endif

#include <stdlib.h>
static inline void _lassert(const char* condition, const char* file, const char* function, int line) {
	fprintf(stderr, "\ncondition %s failed in function \"%s\" on line %d in %s\n", condition, function, line, file);
	exit(1);
}
template<typename T> inline T* const lmalloc(size_t n_bytes) {
	void* data = malloc(n_bytes);
	lassert(data != nullptr);
	return static_cast<T*>(data);
}
template<typename T> inline void lfree(const T* chunk) {
	lassert(chunk != nullptr);
	free(const_cast<void*>(static_cast<const void*>(chunk)));
}
template<typename T> inline T* const lremalloc(const T* chunk, size_t n_bytes) {
	printf("%d\n %d\n", chunk, n_bytes); fflush(stdout);
	lassert(chunk != nullptr);
	T* result = static_cast<T*>(realloc(static_cast<void*>(const_cast<T*>(chunk)), n_bytes));
	lassert(result != nullptr);
	return result;
}

#define LEPSILON 0.00001
#define LPI 3.1415926535897932384626433832795028841971693993751058209749445923078164062
constexpr float lpi = (float)LPI;
inline float lrad(float deg) { return deg*lpi/180.0f; }
inline float lrandf() { return (float)rand()/(float)RAND_MAX; }
template<typename T> inline const T& lmin(const T& a, const T& b) { return b < a ? b : a; }
template<typename T> inline const T& lmax(const T& a, const T& b) { return b > a ? b : a; }
template<typename T> inline const T& lclamp(const T& lower, const T& x, const T& upper) { return lmin(upper, lmax(lower, x)); }
template<typename T> void lswap(T& a, T& b) {
	T tmp = a;
	a = b;
	b = tmp;
}
template<typename T> T labs(const T& value) {
	return (value < 0.0f) ? -value : value;
}
inline bool leq(float a, float b, float threshold = LEPSILON) {
	return labs(a - b) < threshold;
}
inline bool leq(const glm::vec3& a, const glm::vec3& b, float threshold = LEPSILON) {
	return 
		leq(a.x, b.x, threshold) && 
		leq(a.y, b.y, threshold) &&
		leq(a.z, b.z, threshold);
}
inline bool leq(const glm::quat& a, const glm::quat& b, float threshold = LEPSILON) {
	return
		leq(a.x, b.x, threshold) && 
		leq(a.y, b.y, threshold) && 
		leq(a.z, b.z, threshold) && 
		leq(a.w, b.w, threshold);
}

inline std::string lload_file(const char* file) {
	std::string contents;
	FILE* fp = fopen(file, "rb");
	if(fp) {
		DEFER(fclose(fp));
		fseek(fp, 0, SEEK_END);
		size_t size = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		contents.resize(size);
		fread(&contents[0], size, 1, fp);
	}
	return contents;
}


template<u32 x> struct lpopcount_t {
	enum {
		a = x -((x >> 1)     & 0x55555555),
		b =  (((a >> 2)      & 0x33333333) + (a & 0x33333333)),
		c =  (((b >> 4) + b) & 0x0f0f0f0f),
		d =   c + (c >> 8),
		e =   d + (d >> 16),
		result = e & 0x0000003f 
	};
};

template<u32 x> struct llog2_t {
	enum {   
		a = x |(x >> 1),
		b = a |(a >> 2),
		c = b |(b >> 4),
		d = c |(c >> 8),
		e = d |(d >> 16),
		f = e >> 1,
		result = lpopcount_t<f>::result
	};
};

template<int64_t min, int64_t max> struct lbits_required_t {
	static const u32 result = (min == max) ? 0 : llog2_t<u32(max - min)>::result + 1;
};

inline u32 lpopcount(u32 x) {
	const u32 a = x -((x >> 1)     & 0x55555555);
	const u32 b =  (((a >> 2)      & 0x33333333) + (a & 0x33333333));
	const u32 c =  (((b >> 4) + b) & 0x0f0f0f0f);
	const u32 d =   c + (c >> 8);
	const u32 e =   d + (d >> 16);
	const u32 result = e & 0x0000003f;
	return result;
}

#ifdef __GNUC__
inline int lbits_required(u32 min, u32 max) {
	return 32 - __builtin_clz(max - min);
}
#else
inline u32 llog2(u32 x) {
	const u32 a = x |(x >> 1);
	const u32 b = a |(a >> 2);
	const u32 c = b |(b >> 4);
	const u32 d = c |(c >> 8);
	const u32 e = d |(d >> 16);
	const u32 f = e >> 1;
	return lpopcount(f);
}
inline int lbits_required(u32 min, u32 max) {
	return(min == max) ? 0 : llog2(max - min) + 1;
}
#endif

#define CPU_LITTLE_ENDIAN 1
#define CPU_BIG_ENDIAN 2
#if    defined(__386__) || defined(i386)    || defined(__i386__)  \
    || defined(__X86)   || defined(_M_IX86)                       \
    || defined(_M_X64)  || defined(__x86_64__)                    \
    || defined(alpha)   || defined(__alpha) || defined(__alpha__) \
    || defined(_M_ALPHA)                                          \
    || defined(ARM)     || defined(_ARM)    || defined(__arm__)   \
    || defined(WIN32)   || defined(_WIN32)  || defined(__WIN32__) \
    || defined(_WIN32_WCE) || defined(__NT__)                     \
    || defined(__MIPSEL__)
	#define CPU_ENDIAN CPU_LITTLE_ENDIAN
#else
	#define CPU_ENDIAN CPU_BIG_ENDIAN
#endif

inline u32 lhost_to_network(u32 value) {
#if CPU_ENDIAN == CPU_BIG_ENDIAN
	return __builtin_bswap32(value);
#else
	return value;
#endif
}

inline u32 lnetwork_to_host(u32 value) {
#if CPU_ENDIAN == CPU_BIG_ENDIAN
	return __builtin_bswap32(value);
#else
	return value;
#endif
}

inline u8 lhost_to_network(u8 value) {
#if CPU_ENDIAN == CPU_BIG_ENDIAN
	return __builtin_bswap8(value);
#else
	return value;
#endif
}

inline u8 lnetwork_to_host(u8 value) {
#if CPU_ENDIAN == CPU_BIG_ENDIAN
	return __builtin_bswap8(value);
#else
	return value;
#endif
}
