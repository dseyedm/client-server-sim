#pragma once

#include "common.h"

struct model_t {
	// loaded
	float* vertices = NULL;
	float* texcoords = NULL;
	float* normals = NULL;
	
	// calculated
	u16* indices = NULL;

	u32 n_indices = 0;
	u32 n_vertices = 0;

	bool load_from_obj(const char* file);

	~model_t() { destroy(); }
	void destroy() { 
		if(vertices)   free(vertices);
		if(texcoords)  free(texcoords);
		if(normals)    free(normals);
		if(indices)    free(indices);
	}
};
