// note: lhash() = fnv1a implementation.

#pragma once

#include "common.h"

// see @ isthe.com/chongo/tech/comp/fnv/
const uint32_t g_prime = 0x01000193;
const uint32_t g_seed  = 0x811C9DC5;

inline u32 lhash(u8 d, u32 h = g_seed) { return (d^h)*g_prime; }
inline u32 lhash(u16 d, u32 h = g_seed) {
	const u8* p = (const u8*)&d;
	h = lhash(p[0], h);
	return lhash(p[1], h);
}
inline u32 lhash(u32 d, u32 h = g_seed) {
	const u8* p = (const u8*)&d;
	h = lhash(p[0], h);
	h = lhash(p[1], h);
	h = lhash(p[2], h);
	return lhash(p[3], h);
}
inline u32 lhash(const void* d, u32 n_bytes, u32 h = g_seed) {
	const u8* p = (const u8*)d;
	while(n_bytes-- > 0) {
//		h = lhash(*p++, h);
		h = ((*p++)^h)*g_prime;
	}
	return h;
}
inline u32 lhash(const char* s, u32 h = g_seed) {
	while(*s) {
//		h = lhash(*s++, h);
		h = ((*s++)^h)*g_prime;
	}
	return h;
}
inline u32 lhash(f32 d, u32 h = g_seed) { return lhash(&d, sizeof(d), h); }
inline u32 lhash(f64 d, u32 h = g_seed) { return lhash(&d, sizeof(d), h); }
