#include "ezgl.h"

#include <string>
#include <sstream>
#include <set>
#include <string.h>
using namespace std;

static const char* prefix = "#pragma"; const size_t prefix_strlen = strlen(prefix);
static const char* include = "include";
static const char* vertex_zone = "vertex";
static const char* fragment_zone = "fragment";

string shader_t::compile(const char* source) {
	gl(ShaderSource(handle, 1, &source, NULL));
	gl(CompileShader(handle));
	GLint compiled = GL_FALSE;
	gl(GetShaderiv(handle, GL_COMPILE_STATUS, &compiled));
	string info;
	if(compiled != GL_TRUE) {
		GLint info_size;
		gl(GetShaderiv(handle, GL_INFO_LOG_LENGTH, &info_size));
		info.resize(info_size);
		gl(GetShaderInfoLog(handle, info_size, NULL, &info[0]));
	}
	return info;
}
string shader_t::get_source() {
	GLsizei src_size;
	gl(GetShaderiv(handle, GL_SHADER_SOURCE_LENGTH, &src_size));
	string src; src.resize(src_size);
	gl(GetShaderSource(handle, src_size, NULL, &src[0]));
	return src;
}
string program_t::link(const vertex_t& vertex, const fragment_t& fragment) {
	gl(AttachShader(handle, vertex));
	gl(AttachShader(handle, fragment));
	gl(LinkProgram(handle));
	gl(DetachShader(handle, vertex));
	gl(DetachShader(handle, fragment));
	GLint linked = GL_FALSE;
	gl(GetProgramiv(handle, GL_LINK_STATUS, &linked));
	string info;
	if(linked != GL_TRUE) {
		GLint info_size;
		gl(GetProgramiv(handle, GL_INFO_LOG_LENGTH, &info_size));
		info.resize(info_size);
		gl(GetProgramInfoLog(handle, info_size, NULL, &info[0]));
	}
	return info;
}

set<string> _expand_includes_seen;
static void expand_includes(string* ptr_glsl, const char* glsl_path, bool root = true) {
	if(_expand_includes_seen.find(glsl_path) != _expand_includes_seen.end()) {
		// todo: have this retrace the tree.
		fprintf(stderr, "warning: multiple \"%s %s %s\" detected.\n", prefix, include, glsl_path);
		ptr_glsl->clear();
		return;
	}
	_expand_includes_seen.insert(glsl_path);
	const string& original = *ptr_glsl;
	string expanded, line;
	stringstream ss; ss << original;
	while(getline(ss, line)) {
		bool did_expand = false;
		do {
			if(line.size() < prefix_strlen) break;
			size_t match_prefix;
			for(match_prefix = 0; match_prefix < prefix_strlen; ++match_prefix) {
				if(prefix[match_prefix] != line[match_prefix]) break;
			}
			if(match_prefix < prefix_strlen) break;

			string word;
			size_t p;
			for(p = match_prefix; p < line.size(); ++p) {
				if(isspace(line[p])) {
					if(word.size()) break;
				} else word += line[p];
			}
			if(word != include) break;

			size_t first_comma = string::npos, second_comma = string::npos;
			for(size_t m = p; m < line.size(); ++m) {
				if(line[m] != '"') continue;
				if(first_comma == string::npos) first_comma = m;
				else second_comma = m;
			}

			string path;
			if(first_comma == string::npos || second_comma == string::npos) {
				path = line.substr(p, line.size() - p);
			} else {
				path = line.substr(first_comma + 1, second_comma - first_comma - 1);
			}

			string ptr_inner_content = lload_file(path.c_str());
			if(!ptr_inner_content.size()) {
				fprintf(stderr, "warning: failed to load path \"%s\" in %s\n", path.c_str(), glsl_path);
			} else {
				string inner_content = ptr_inner_content;
				expand_includes(&inner_content, path.c_str(), false);
				expanded += inner_content;
			}
			
			did_expand = true;
		} while(false);
		if(!did_expand) {
			expanded += line; expanded += '\n';
		}
	}
	*ptr_glsl = expanded;
	if(root) _expand_includes_seen.clear();
}
bool program_load_files(program_t* program, const char* vertex_path, const char* fragment_path) {
	string content = lload_file(vertex_path); 
	if(!content.size()) {
		fprintf(stderr, "failed to find file %s\n", vertex_path);
		return false;
	}
	
	vertex_t vertex; vertex.create(); DEFER(vertex.destroy());
	string result = vertex.compile(content.c_str());
	if(result.size()) {
		fprintf(stderr, "failed to load vertex shader %s:\n%s", vertex_path, result.c_str());
		return false;
	}

	content = lload_file(fragment_path);
	if(!content.size()) {
		fprintf(stderr, "failed to find file %s\n", fragment_path);
		return false;
	}
	fragment_t fragment; fragment.create(); DEFER(fragment.destroy());
	result = fragment.compile(content.c_str());
	if(result.size()) {
		fprintf(stderr, "failed to load fragment shader %s:\n%s", fragment_path, result.c_str());
		return false;
	}

	result = program->link(vertex, fragment);
	if(result.size()) {
		fprintf(stderr, "failed to link program from shaders:\n%s\n%s:\n%s", vertex_path, fragment_path, result.c_str());
		return false;
	}
	return true;
}

bool program_load_file(program_t* program, const char* glsl_path) {
	string content = lload_file(glsl_path);
	if(!content.size()) {
		fprintf(stderr, "failed to find file %s\n", glsl_path);
		return false;
	}

	// metadata parser
	size_t vertex_zone_start = string::npos;
	size_t vertex_zone_start_endl = string::npos;
	size_t fragment_zone_start = string::npos;
	size_t fragment_zone_start_endl = string::npos;
	size_t next_search_start = 0;
	do {
		size_t prefix_start = content.find(prefix, next_search_start);
		if(prefix_start == string::npos) break;
		string word;
		for(size_t z = prefix_start + prefix_strlen; z < content.size(); ++z) {
			if(content[z] < ' ' || content[z] > '~') break;
			if(isspace(content[z])) {
				if(word.size()) break;
				continue;
			} 
			word += content[z];
		}
		size_t end_of_the_line;
		for(end_of_the_line = prefix_start + prefix_strlen; end_of_the_line < content.size(); ++end_of_the_line) {
			if(content[end_of_the_line] < ' ' || content[end_of_the_line] > '~') break;
		}
		next_search_start = end_of_the_line;
		if(word == vertex_zone) {
			if(vertex_zone_start != string::npos) {
				fprintf(stderr, "failed to load file %s:\ntoo many vertex zones.\n", glsl_path);
				return false;
			}
			vertex_zone_start = prefix_start;
			vertex_zone_start_endl = end_of_the_line;
		} else if(word == fragment_zone) {
			if(fragment_zone_start != string::npos) {
				fprintf(stderr, "failed to load file %s:\ntoo many fragment zones.\n", glsl_path);
				return false;
			}
			fragment_zone_start = prefix_start;
			fragment_zone_start_endl = end_of_the_line;
		}
	} while(1);

	if(vertex_zone_start == string::npos || fragment_zone_start == string::npos) {
		fprintf(stderr, "failed to load file %s:\nno \"%s %s\" was found.\n", glsl_path, prefix, (vertex_zone_start == string::npos ? vertex_zone : fragment_zone));
		return false;
	}
	
	string vertex_content, fragment_content; {
		size_t vertex_zone_size, fragment_zone_size;
		if(vertex_zone_start < fragment_zone_start) {
			vertex_zone_size = fragment_zone_start - vertex_zone_start_endl;
			fragment_zone_size = content.size() - fragment_zone_start_endl;
		} else {
			fragment_zone_size = vertex_zone_start - fragment_zone_start_endl;
			vertex_zone_size = content.size() - vertex_zone_start_endl;
		}
		vertex_content = content.substr(vertex_zone_start_endl, vertex_zone_size).c_str();
		fragment_content = content.substr(fragment_zone_start_endl, fragment_zone_size).c_str();
	}

	expand_includes(&vertex_content, glsl_path);
	vertex_t vertex; vertex.create(); DEFER(vertex.destroy());
	string result = vertex.compile(vertex_content.c_str());
	if(result.size()) {
		fprintf(stderr, "failed to load vertex shader (zone) in %s:\n%s", glsl_path, result.c_str());
		return false;
	}

	expand_includes(&fragment_content, glsl_path);
	fragment_t fragment; fragment.create(); DEFER(fragment.destroy());
	result = fragment.compile(fragment_content.c_str());
	if(result.size()) {
		fprintf(stderr, "failed to load fragment shader (zone) in %s:\n%s", glsl_path, result.c_str());
		return false;
	}
	result = program->link(vertex, fragment);
	if(result.size()) {
		fprintf(stderr, "failed to link program from shader %s:\n%s", glsl_path, result.c_str());
		return false;
	}
	return true;
}
