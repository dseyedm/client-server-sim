#define RENDER_BOTH 0
#define RENDER_CLIENT 1

#define RENDER_WHAT RENDER_BOTH

static constexpr int cubes_len = 30;
static constexpr int n_entities = 1/* player */ + cubes_len*cubes_len;
namespace sv {
	float sv_netcheckrate_s = 1.0f/60.0f;
	float sv_simrate_s = 1.0f/30.0f;
	float sv_fake_tt_ms = 100.0f;
	float sv_fake_loss = 0.00f;
	float sv_fake_jitter = 0.00f;
	static constexpr int sv_buffer = 64;
}
namespace cl {
	float cl_netsendrate_s = 1.0f/60.0f;
	float cl_netcheckrate_s = 1.0f/60.0f;
	float cl_smooth = 0.15f;
	static constexpr int client_jitter = 3;
}
int resolution_x = 1920/2; 
int resolution_y = 1080/2;
float middle_x = (float)resolution_x/2.0f;
float middle_y = (float)resolution_y/2.0f;
float cursor_x = middle_x, cursor_y = middle_y;
const float player_scale = 12.0f;
const float cube_scale = 6.0f;
float player_mass = 3*1.5f;
float cube_mass = 1.5f;
float player_impulse = 25.0f;
float cube_attract = 15.0f;
float cube_detract = 38.0f;
float cube_min_radius = 5.0f;
float cube_max_radius = 25.0f;
float cam_depth = 60.0f;
float gravity = -60.0f;

#define PF(v)    printf(#v " %f\n", v);
#define PVEC3(v) printf(#v " %f %f %f\n", v.x, v.y, v.z);
inline bool accum_do(float* accum, float limit) {
	if(*accum >= limit) { \
		while(*accum >= limit) *accum -= limit;
		return true;
	}
	return false;
}

#include "ezglext.h"
#include "model.h"
#include "lserialize.h"

#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw_gl3.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

#include <GLFW/glfw3.h>
#include <bullet/btBulletCollisionCommon.h>
#include <BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h>
#include <BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolver.h>
#include <enet/enet.h>

#include <pthread.h>
#include <vector>
#include <stdint.h>
#include <time.h>
#include <stdlib.h>
#include <sstream>
using namespace std;


void glfw_error_callback(int error, const char* description) { fprintf(stderr, "GLFW3 error %d: %s\n", error, description); }

inline vec3 toglm(const btVector3& v) { return vec3(v.x(), v.y(), v.z()); }
inline btVector3 tobt(const vec3& v) { return btVector3(v.x, v.y, v.z); }
inline quat toglm(const btQuaternion& v) { return quat(v.w(), v.x(), v.y(), v.z()); }
inline btQuaternion tobt(const quat& v) { return btQuaternion(v.x, v.y, v.z, v.w); }

const vec3 axis_x = vec3(1.0f, 0.0f, 0.0f);
const vec3 axis_y = vec3(0.0f, 1.0f, 0.0f);
const vec3 axis_z = vec3(0.0f, 0.0f, 1.0f);

#define GET_UNIFORM(S, U) \
	S##_##U = (GLuint)gl(GetUniformLocation(S, #U)); \
	if(S##_##U == IHANDLE) { fprintf(stderr, "warning: '" #S "' shader does not contain uniform '" #U "'\n"); }
struct common_uniform_direct_t {
	const char* name;
	GLenum type;
	void* data;
};
struct common_attribute_t {
	const char* name;
	GLenum type;
	GLuint location;
};
enum {
	LOCATION_VERTEX,
	LOCATION_NORMAL,
	LOCATION_TEXCOORD,
};
const common_attribute_t common_attributes[] = {
	{ "vertex", GL_FLOAT_VEC3, LOCATION_VERTEX },
	{ "normal", GL_FLOAT_VEC3, LOCATION_NORMAL },
	{ "texcoord", GL_FLOAT_VEC2, LOCATION_TEXCOORD },
};

struct mesh_t {
	vbo_t index_vbo;
	vbo_t vertex_vbo;
	vbo_t normal_vbo;
	vbo_t texcoord_vbo;
	vao_t vao;
	int n = 0;
	GLenum mode = GL_TRIANGLES;

	inline void create() { index_vbo.create(); vertex_vbo.create(); normal_vbo.create(); texcoord_vbo.create(); vao.create(); }
	inline void destroy() { index_vbo.destroy(); vertex_vbo.destroy(); normal_vbo.destroy(); texcoord_vbo.destroy(); vao.destroy(); }
};

struct mesh_obj_pair_t { 
	mesh_t* mesh;
	const char* obj_path;
};

mesh_t mcube;
mesh_t mplane;
mesh_t mteapot;
mesh_t mtorus;
mesh_t msuzanne;
static const mesh_obj_pair_t mesh_obj_pairs[] = {
	{ &mcube, "cube.obj" },
	{ &mplane, "xzplane.obj" },
	{ &mteapot, "teapot.obj" },
	{ &mtorus, "xztorus.obj" },
	{ &msuzanne, "suzanne.obj" },
};

#define NEW_UNIFORM(S, U) \
	GLuint S##_##U = IHANDLE
	
program_ext_t raster;
NEW_UNIFORM(raster, model);
NEW_UNIFORM(raster, model_view_projection);

program_ext_t postfx;
NEW_UNIFORM(postfx, left);
NEW_UNIFORM(postfx, right);

program_ext_t* programs[] = { &raster, &postfx };
#undef NEW_UNIFORM

texture2d_t color_a;
texture2d_t depth_a;
fbo_t fbo_a;

texture2d_t color_b;
texture2d_t depth_b;
fbo_t fbo_b;

void reload_buffers() { 
	texture2d_t* colors[] = { &color_a, &color_b };
	texture2d_t* depths[] = { &depth_a, &depth_b };
	fbo_t* fbos[] = { &fbo_a, &fbo_b };
	
	for(size_t i = 0; i < LENGTH(fbos); ++i) {
		texture2d_t& color = *colors[i];
		texture2d_t& depth = *depths[i];
		fbo_t& fbo = *fbos[i];
		
		color.create();
		color.bind();
		color.set_filter(GL_NEAREST);
		color.set_wrap(GL_REPEAT);
#if RENDER_WHAT == RENDER_BOTH
		color.upload(NULL, resolution_x/2, resolution_y, GL_UNSIGNED_BYTE, GL_RGBA, GL_RGBA8);
#else
		color.upload(NULL, resolution_x, resolution_y, GL_UNSIGNED_BYTE, GL_RGBA, GL_RGBA8);
#endif
		color.unbind();

		depth.create();
		depth.bind();
		depth.set_filter(GL_NEAREST);
		depth.set_wrap(GL_REPEAT);
#if RENDER_WHAT == RENDER_BOTH
		depth.upload(NULL, resolution_x/2, resolution_y, GL_UNSIGNED_BYTE, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT24);
#else
		depth.upload(NULL, resolution_x, resolution_y, GL_UNSIGNED_BYTE, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT24);
#endif
		depth.unbind();

		fbo.create();
		fbo.bind();
		fbo.attach(color, GL_COLOR_ATTACHMENT0);
		fbo.attach(depth, GL_DEPTH_ATTACHMENT);
		lassert(fbo.status() == GL_FRAMEBUFFER_COMPLETE);
		fbo.unbind();
	}
}

void reload_textures() { 
#if 0
	stbi_set_flip_vertically_on_load(true);
	int w, h, components;
	uint8_t* data;
	// todo: use table driven design to reduce c&p
	data = stbi_load("lens_star.png", &w, &h, &components, STBI_rgb_alpha);
	if(data) {
		lens_star.create();
		lens_star.bind();
		lens_star.upload(&data[0], w, h, GL_UNSIGNED_BYTE, GL_RGBA, GL_RGBA8);
		lens_star.set_filter(GL_LINEAR);
		lens_star.set_wrap(GL_CLAMP_TO_EDGE);
		lens_star.unbind();
		stbi_image_free(data);
	} else {
		fprintf(stderr, "failed to load texture %s\n", "lens_star.png");
	}

	data = stbi_load("metal_normal.png", &w, &h, &components, STBI_rgb_alpha);
	if(data) {
		metal_normal.create();
		metal_normal.bind();
		metal_normal.upload(&data[0], w, h, GL_UNSIGNED_BYTE, GL_RGBA, GL_RGBA8);
		metal_normal.set_filter(GL_LINEAR);
		metal_normal.set_wrap(GL_CLAMP_TO_EDGE);
		metal_normal.unbind();
		stbi_image_free(data);
	} else {
		fprintf(stderr, "failed to load texture %s\n", "metal_normal.png");
	}
#endif
}

void reload_meshes() {
	for(size_t i = 0; i < LENGTH(mesh_obj_pairs); ++i) {
		model_t model; lassert(model.load_from_obj(mesh_obj_pairs[i].obj_path));
		mesh_t& m = *mesh_obj_pairs[i].mesh;
		m.create();
		m.vao.bind();
		m.n = model.n_indices;
		// todo: use table driven design to reduce c&p
		m.index_vbo.bind(GL_ELEMENT_ARRAY_BUFFER);
		gl(BufferData(
			GL_ELEMENT_ARRAY_BUFFER,
			model.n_indices*sizeof(*model.indices),
			model.indices,
			GL_STATIC_DRAW
		));
		m.vertex_vbo.bind(GL_ARRAY_BUFFER);
		gl(BufferData(
			GL_ARRAY_BUFFER,
			model.n_vertices*3*sizeof(*model.vertices),
			model.vertices,
			GL_STATIC_DRAW
		));
		m.normal_vbo.bind(GL_ARRAY_BUFFER);
		gl(BufferData(
			GL_ARRAY_BUFFER,
			model.n_vertices*3*sizeof(*model.normals),
			model.normals,
			GL_STATIC_DRAW
		));
		m.texcoord_vbo.bind(GL_ARRAY_BUFFER);
		gl(BufferData(
			GL_ARRAY_BUFFER,
			model.n_vertices*2*sizeof(*model.texcoords),
			model.texcoords,
			GL_STATIC_DRAW
		));
		for(int o = 0; o < (int)LENGTH(programs); ++o) {
			program_ext_t& pr = *programs[o];
			for(size_t e = 0; e < pr.data.attributes.size(); ++e) {
				program_attribute_t& at = pr.data.attributes[e];
				for(int i = 0; i < (int)LENGTH(common_attributes); ++i) {
					const common_attribute_t& ca = common_attributes[i];
					if(ca.name != at.name || ca.type != at.type)
						continue;

					gl(EnableVertexAttribArray(ca.location));
					
					if(ca.location == LOCATION_VERTEX) { m.vertex_vbo.bind(GL_ARRAY_BUFFER); } 
					else if(ca.location == LOCATION_NORMAL) { m.normal_vbo.bind(GL_ARRAY_BUFFER); } 
					else if(ca.location == LOCATION_TEXCOORD) { m.texcoord_vbo.bind(GL_ARRAY_BUFFER); } 
					else { lassert("ca.location is invalid." == NULL); }
					
					if(ca.location == LOCATION_TEXCOORD) { gl(VertexAttribPointer(at.handle, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0)); } 
					else { gl(VertexAttribPointer(at.handle, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0)); }
				}
			}
		}
	}
}

void reload_shaders(const common_uniform_direct_t* commons, int n_commons) {
	{
		program_t candidate_raster; candidate_raster.create();
		if(!program_load_file(&candidate_raster, "raster.glsl")) {
			candidate_raster.destroy();
		} else {
			raster.destroy();
			raster = candidate_raster;
			GET_UNIFORM(raster, model);
			GET_UNIFORM(raster, model_view_projection);
		}
	}
	{
		program_t candidate_postfx; candidate_postfx.create();
		if(!program_load_file(&candidate_postfx, "postfx.glsl")) {
			candidate_postfx.destroy();
		} else {
			postfx.destroy();
			postfx = candidate_postfx;
			GET_UNIFORM(postfx, left);
			GET_UNIFORM(postfx, right);
		}
	}
	for(int p = 0; p < (int)LENGTH(programs); ++p) {
		program_data_t& pd = programs[p]->data;
		pd.create(programs[p]);
		for(size_t u = 0; u < pd.uniforms.size(); ++u) {
			program_uniform_t& uf = pd.uniforms[u];
			for(int c = 0; c < n_commons; ++c) {
				const common_uniform_direct_t& cm = commons[c];
				if(cm.name != uf.name || cm.type != uf.type)
					continue;

				uf.data = cm.data;
			}
		}
	}
}
#undef GET_UNIFORM

void glfw_framebuffer_size_changed(GLFWwindow* window, int width, int height) {
	if(width <= 0) width = 1;
	if(height <= 0) height = 1;
	if(width == resolution_x && height == resolution_y) return;
	resolution_x = width;
	resolution_y = height;
	reload_buffers();
}

const vec3 cam_base_right   = vec3(1.0f, 0.0f, 0.0f);
const vec3 cam_base_up      = vec3(0.0f, 1.0f, 0.0f);
const vec3 cam_base_forward = vec3(0.0f, 0.0f,-1.0f);

struct cam_t {
	// input
	float fovy = lrad(90.0f);
	float near = 0.25f;
	float far = 1000.0f;
	float aspect = (float)resolution_x/(float)resolution_y; 
	vec3 position = 10.0f*vec3(2.0f, 2.0f, 2.0f);
	quat orientation = angleAxis(lrad(-45.0f), axis_y);
	
	cam_t() { calculate(); } // ensure valid state ASAP

	// generated
	vec3 right; 
	vec3 up;
	vec3 forward;
	mat4 projection; 
	mat4 view; 
	mat4 view_projection;
	float tan_half_fovy = tanf(0.5f*fovy);
	
	struct input_t {
		bool press_forward;
		bool press_backward;
		bool press_left;
		bool press_right;
		bool press_local_up;
		bool press_local_down;
		bool press_global_up;
		bool press_global_down;
		bool press_roll_left;
		bool press_roll_right;
		bool press_esc;	
	};
	
	void process_mouselook(float dts, GLFWwindow* window) {
		glfwSetCursorPos(window, middle_x, middle_y);
		const float rads_per_px_dts = lrad(10.0f)*dts;
		float diff_x = (cursor_x - 0.5f*resolution_x)*rads_per_px_dts;
		float diff_y = (cursor_y - 0.5f*resolution_y)*rads_per_px_dts;
		quat qx = angleAxis(diff_y, right);
		quat qy = angleAxis(diff_x, axis_y);
		orientation *= qx;
		orientation *= qy;
	}
	
	void process_movement(float dts, GLFWwindow* window, const input_t& in) {
		// compute roll
		if(in.press_roll_right || in.press_roll_left) {
			float delta = lrad(90.0f)*dts;
			if(in.press_roll_right) delta *= -1.0f;
			quat qz = angleAxis(delta, forward);
			orientation *= qz;
		}

		{ // compute position
			float speed = 100.0f;
			vec3 delta;
			float speed_dts = speed*dts;
			if(in.press_forward || in.press_backward) {
				delta = forward*speed_dts;
				if(in.press_forward)  position += delta;
				if(in.press_backward) position -= delta;
			}
			if(in.press_right || in.press_left) {
				delta = right*speed_dts;
				if(in.press_right) position += delta;
				if(in.press_left)  position -= delta;
			}
			if(in.press_local_up || in.press_local_down) {
				delta = up*speed_dts;
				if(in.press_local_up)   position += delta;
				if(in.press_local_down) position -= delta;
			}
			if(in.press_global_up || in.press_global_down) {
				delta = axis_y*speed_dts;
				if(in.press_global_up)   position += delta;
				if(in.press_global_down) position -= delta;
			}
		}			
	}
	
	void calculate() {
		// common rendering prerequisites
		right   = cam_base_right*orientation;
		up      = cam_base_up*orientation;
		forward = cam_base_forward*orientation;
		tan_half_fovy = tanf(fovy*0.5f);
		projection = perspective(fovy, aspect, near, far);
		view = lookAt(
			position,
			position + forward,
			up
		);
		view_projection = projection*view;
	}
};

struct packet_t {
	vector<u8> data;
};

struct entity_t {
	vec3 origin = vec3(0.0f);
	quat orientation;
	
#if 0
	vec3 velocity = vec3(0.0f);
	vec3 angular_velocity = vec3(0.0f);
#endif
	
	entity_t() { }
	entity_t(vec3 _origin, quat _orientation) { origin = _origin; orientation = _orientation; }
	
#if 0
	SERIALIZE_OBJECT() {
		if(!serialize_float(stream, origin.x)) return false;
		if(!serialize_float(stream, origin.y)) return false;
		if(!serialize_float(stream, origin.z)) return false;
		if(!serialize_float(stream, orientation.x)) return false;
		if(!serialize_float(stream, orientation.y)) return false;
		if(!serialize_float(stream, orientation.z)) return false;
		if(!serialize_float(stream, orientation.w)) return false;
		return true;
	}
#else
	SERIALIZE_OBJECT() {
		const entity_t* this_delta = (const entity_t*)user_ptr;
	
		bool at_rest;
		if(stream_type::is_writing) {
			at_rest = 
				leq(origin, this_delta->origin) &&
				leq(orientation, this_delta->orientation);
#if 0
				leq(velocity, this_delta->velocity) &&
				leq(angular_velocity, this_delta->angular_velocity);
#endif
		}
		if(!serialize_bool(stream, at_rest)) return false;
		if(!at_rest) {
			if(!serialize_compressed_float(stream, origin.x, -1024.0f, 1024.0f, 0.01f)) return false;
			if(!serialize_compressed_float(stream, origin.y, 0.0f, 1024.0f, 0.01f)) return false;
			if(!serialize_compressed_float(stream, origin.z, -1024.0f, 1024.0f, 0.01f)) return false;
			lcompressed_quat_t<7> cq;
			if(stream_type::is_writing) {
				cq.compress(orientation.x, orientation.y, orientation.z, orientation.w);
			}
			if(!serialize_object(stream, cq)) return false;
			if(stream_type::is_reading) {
				cq.uncompress(&orientation.x, &orientation.y, &orientation.z, &orientation.w);
				orientation = normalize(orientation);
			}
#if 0
			if(!serialize_compressed_float(stream, velocity.x, -4.0f, 4.0f, 0.1f)) return false;
			if(!serialize_compressed_float(stream, velocity.y, -4.0f, 4.0f, 0.1f)) return false;
			if(!serialize_compressed_float(stream, velocity.z, -4.0f, 4.0f, 0.1f)) return false;
			if(!serialize_compressed_float(stream, angular_velocity.x, -4.0f, 4.0f, 0.1f)) return false;
			if(!serialize_compressed_float(stream, angular_velocity.y, -4.0f, 4.0f, 0.1f)) return false;
			if(!serialize_compressed_float(stream, angular_velocity.z, -4.0f, 4.0f, 0.1f)) return false;
#endif
		}
		return true;
	}
#endif
};

struct renderstate_t {
	// note: please, please, please keep this data layout _flat_!
	
	// dynamic
	float timestamp;
	entity_t player;
	entity_t cubes[cubes_len*cubes_len];
	
	void render(const mat4& view_projection) {
		static entity_t eplane(vec3(0.0f), quat());
		
		raster.bind();
		raster.data.set_uniforms();
		mesh_t* last_mesh = NULL;
		for(size_t i = 0; i < 2 + cubes_len*cubes_len; ++i) {
			entity_t& e = i == 0 ? eplane : (i == 1 ? player : cubes[i - 2]);
			mesh_t& m = i == 0 ? mplane : mcube;
			vec3 e_scale = i == 0 ? vec3(100000.0f) : (i == 1 ? vec3(player_scale) : vec3(cube_scale));
			
			if(&m != last_mesh) {
				if(last_mesh) last_mesh->vao.unbind();
				m.vao.bind();
			}
			
			if(raster_model != IHANDLE) {
				mat4 mvp;
				mvp = translate(mvp, e.origin);
				mvp = scale(mvp, e_scale);
				mvp *= mat4(e.orientation);
				gl(UniformMatrix4fv(raster_model, 1, GL_FALSE, (float*)&mvp));
			}
			if(raster_model_view_projection != IHANDLE) {
				mat4 mvp;
				mvp = translate(mvp, e.origin);
				mvp = scale(mvp, e_scale);
				mvp *= mat4(e.orientation);
				mvp = view_projection*mvp;
				gl(UniformMatrix4fv(raster_model_view_projection, 1, GL_FALSE, (float*)&mvp));
			}

			gl(DrawElements(m.mode, m.n, GL_UNSIGNED_SHORT, NULL));
			last_mesh = &m;
		}
		if(last_mesh) last_mesh->vao.unbind();
		raster.unbind();
	}
	
	struct serialize_args_t { 
		int this_rsbuffer_id; // in/out
		const renderstate_t* deltas = NULL; // in
		int deltas_length = 0; // in
		int delta_rsbuffer_id = 0; // in/out
	};
	SERIALIZE_OBJECT() {
		serialize_args_t* sa = (serialize_args_t*)user_ptr;
		lassert(sa);
		
		if(!serialize_float(stream, timestamp)) return false;
		if(!serialize_compressed_int(stream, sa->this_rsbuffer_id, 0, sv::sv_buffer - 1)) return false;
		if(!serialize_compressed_int(stream, sa->delta_rsbuffer_id, 0, sv::sv_buffer - 1)) return false;
		if(stream_type::is_reading) {
			if(sa->delta_rsbuffer_id < 0 || sa->delta_rsbuffer_id >= sa->deltas_length) return false; 
			memmove(&this->player, &sa->deltas[sa->delta_rsbuffer_id].player, sizeof(this->player));
			memmove(&this->cubes, &sa->deltas[sa->delta_rsbuffer_id].cubes, sizeof(this->cubes));
		}
		
		if(!serialize_object(stream, player, (void*)&sa->deltas[sa->delta_rsbuffer_id].player)) return false;
		for(size_t z = 0; z < cubes_len*cubes_len; ++z) {
			if(!serialize_object(stream, cubes[z], (void*)&sa->deltas[sa->delta_rsbuffer_id].cubes[z])) return false;
		}
		return true;
	}

	packet_t to_packet(int rsbuffer_i, const renderstate_t* rsbuffer, int rsbuffer_length, int in_delta_id) {
		serialize_args_t sa;
		sa.this_rsbuffer_id = rsbuffer_i;
		sa.deltas = rsbuffer;
		sa.deltas_length = rsbuffer_length;
		sa.delta_rsbuffer_id = in_delta_id;
		
		packet_t packet;
		packet.data.resize(sizeof(float) + sizeof(entity_t)*n_entities);
		lwrite_stream_t ws(packet.data.data(), packet.data.size()); lassert(serialize_object(ws, *this, &sa)); ws.flush();
		packet.data.resize(ws.words_written()*4);
		return packet;
	}
	bool set_from_packet(const packet_t& packet, const renderstate_t* rsbuffer, int rsbuffer_length, int* out_this_rsbuffer_id) {
		serialize_args_t sa;
		sa.this_rsbuffer_id = -1;
		sa.deltas = rsbuffer;
		sa.deltas_length = rsbuffer_length;
		sa.delta_rsbuffer_id = -1;
		
		lread_stream_t rs(packet.data.data(), packet.data.size());
		bool good = serialize_object(rs, *this, &sa);
		if(!good) return false;
		*out_this_rsbuffer_id = sa.this_rsbuffer_id;
		return true;
	}
	
	void lerp(const renderstate_t& a, const renderstate_t& b, float t) {
		timestamp = glm::lerp(a.timestamp, b.timestamp, t);
		player.origin = glm::lerp(a.player.origin, b.player.origin, t);
		player.orientation = slerp(a.player.orientation, b.player.orientation, t);
		for(size_t i = 0; i < cubes_len*cubes_len; ++i) {
			cubes[i].origin = glm::lerp(a.cubes[i].origin, b.cubes[i].origin, t);
			cubes[i].orientation = slerp(a.cubes[i].orientation, b.cubes[i].orientation, t);
		}
	}
};

template<int S> struct jitter_t {
	renderstate_t snapshots[S];
	int size = 0;
	
	bool add(const renderstate_t& new_rs, float alpha_timestamp = -1.0f, renderstate_t* out_popped = NULL) {
		if(new_rs.timestamp < alpha_timestamp) { return false; }
		if(size + 1 <= S) {
			snapshots[size] = new_rs;
			for(int i = size; i >= 0; --i)  {
				if(i == 0 || new_rs.timestamp > snapshots[i - 1].timestamp) {
					snapshots[i] = new_rs;
					break;
				}
				snapshots[i] = snapshots[i - 1];
			}
			++size;
			return false;
		}
		
		if(new_rs.timestamp < snapshots[0].timestamp) {
			*out_popped = new_rs;
			return true;
		}
		*out_popped = snapshots[0];
		
		snapshots[0] = new_rs;
		for(int i = 1; i < size; ++i)  {
			snapshots[i - 1] = snapshots[i];
			if(i + 1 >= size || new_rs.timestamp < snapshots[i + 1].timestamp) {
				snapshots[i] = new_rs;
				break;
			}
		}
		return true;
	}
};

typedef btRigidBody::btRigidBodyConstructionInfo btRigidBodyConstructionInfo;
struct simstate_t {
	btBroadphaseInterface* bphase = NULL;
	btDefaultCollisionConfiguration* ccfg = NULL;
	btCollisionDispatcher* dispatch = NULL;
	btSequentialImpulseConstraintSolver* solver = NULL;
	btDiscreteDynamicsWorld* dworld = NULL; 
	
	btStaticPlaneShape* floor_shape = NULL; 
	btDefaultMotionState* floor_mstate = NULL; 
	btRigidBody* floor_rigid = NULL; 
	
	btBoxShape* player_shape = NULL;
	btDefaultMotionState* player_mstate = NULL;
	btRigidBody* player_rigid = NULL;

	btBoxShape* cube_shape = NULL;
	btMotionState* cube_mstates[cubes_len*cubes_len];
	btRigidBody* cube_rigids[cubes_len*cubes_len];
	float timestamp = 0.0f;

	simstate_t() {
		bphase = new btDbvtBroadphase();
		ccfg = new btDefaultCollisionConfiguration();
		dispatch = new btCollisionDispatcher(ccfg);
		solver = new btSequentialImpulseConstraintSolver();
		dworld = new btDiscreteDynamicsWorld(dispatch, bphase, solver, ccfg);
		
		dworld->setGravity(btVector3(0, gravity, 0));
		
		{ // floor
			floor_shape = new btStaticPlaneShape(btVector3(0.0f, 1.0f, 0.0f), 1.0f);
			floor_mstate = new btDefaultMotionState(btTransform(btQuaternion(0.0f, 0.0f, 0.0f, 1.0f), btVector3(0.0f, 1.0f, 0.0f)));
			btRigidBodyConstructionInfo floor_cinfo(0.0f, floor_mstate, floor_shape);
			floor_rigid = new btRigidBody(floor_cinfo);
			dworld->addRigidBody(floor_rigid);
		}

		{ // player
			player_shape = new btBoxShape(btVector3(player_scale/2.0f, player_scale/2.0f, player_scale/2.0f));
			player_mstate = new btDefaultMotionState(initial_player());
			btVector3 inertia;
			player_shape->calculateLocalInertia(player_mass, inertia);
			btRigidBodyConstructionInfo player_cinfo(player_mass, player_mstate, player_shape, inertia);
			player_cinfo.m_friction = 3.0f;
			player_rigid = new btRigidBody(player_cinfo);
			dworld->addRigidBody(player_rigid);
		}
		
		{ // cubes
			cube_shape = new btBoxShape(btVector3(cube_scale/2.0f, cube_scale/2.0f, cube_scale/2.0f));
			btVector3 inertia;
			cube_shape->calculateLocalInertia(cube_mass, inertia);
			for(size_t i = 0; i < LENGTH(cube_mstates); ++i) {
				cube_mstates[i] = new btDefaultMotionState(initial_cube(i));
				
				btRigidBodyConstructionInfo cube_cinfo = btRigidBodyConstructionInfo(cube_mass, cube_mstates[i], cube_shape, inertia);
				cube_cinfo.m_friction = 3.0f;
				cube_rigids[i] = new btRigidBody(cube_cinfo);
				cube_rigids[i]->setActivationState(WANTS_DEACTIVATION);
				cube_rigids[i]->setRestitution(0.1f);
				dworld->addRigidBody(cube_rigids[i]);
			}
		}
	}
	
	~simstate_t() {
#if 0
		delete floor_shape;
		delete floor_mstate; 
		delete floor_rigid; 
		
		delete player_shape;
		delete player_mstate;
		delete player_rigid;
		
		delete cube_shape;
		for(size_t i = 0; i < LENGTH(cube_rigids); ++i) {
			delete cube_rigids[i];
			delete cube_mstates[i];
		}
		
		delete dworld;
		delete solver;
		delete dispatch;
		delete ccfg;
#endif
	}
	
	btTransform initial_player() { 
		return btTransform(
			btQuaternion(0.0f, 0.0f, 0.0f, 1.0f),
			btVector3(0.0f, 5.5f, 0.0f)
		);
	}
	btTransform initial_cube(int i) {
		return btTransform(
			btQuaternion(0.0f, 0.0f, 0.0f, 1.0f), 
			btVector3(10.0f*((i%cubes_len) - cubes_len/2.0f), 15.0f, 10.0f*((float)i/cubes_len - cubes_len/2.0f))
		);
	}
	void init() {
		player_rigid->clearForces();
		player_rigid->setLinearVelocity(btVector3(0.0f, 0.0f, 0.0f));
		player_rigid->setAngularVelocity(btVector3(0.0f, 0.0f, 0.0f));
		player_rigid->setWorldTransform(initial_player());
		player_mstate->setWorldTransform(player_rigid->getWorldTransform());
		player_rigid->setActivationState(0);
		for(size_t i = 0; i < LENGTH(cube_mstates); ++i) {
			cube_rigids[i]->clearForces();
			cube_rigids[i]->setLinearVelocity(btVector3(0.0f, 0.0f, 0.0f));
			cube_rigids[i]->setAngularVelocity(btVector3(0.0f, 0.0f, 0.0f));
			cube_rigids[i]->setWorldTransform(initial_cube(i));
			cube_mstates[i]->setWorldTransform(cube_rigids[i]->getWorldTransform());
			cube_rigids[i]->setActivationState(0);
		}
	}
	
	struct input_t {
		vec3 forward, right;
		bool press_move_forward;
		bool press_move_backward;
		bool press_move_left;
		bool press_move_right;
		bool press_attract;
		bool press_detract;
	};
	
	// update the sim world "relative" to the current render state.
	void process_input(const renderstate_t& rs, const input_t& in) {
		if(in.press_move_forward || in.press_move_backward || in.press_move_left || in.press_move_right) {
			player_rigid->activate(true);
		}
		if(in.press_move_forward) { player_rigid->applyCentralImpulse(tobt(vec3(1.0f, 0.0f, 1.0f) * in.forward*player_impulse)); }
		if(in.press_move_backward) { player_rigid->applyCentralImpulse(tobt(vec3(1.0f, 0.0f, 1.0f) * -in.forward*player_impulse)); }
		if(in.press_move_left) { player_rigid->applyCentralImpulse(tobt(vec3(1.0f, 0.0f, 1.0f) * -in.right*player_impulse)); }
		if(in.press_move_right) { player_rigid->applyCentralImpulse(tobt(vec3(1.0f, 0.0f, 1.0f) * in.right*player_impulse)); }
		
		for(size_t i = 0; i < cubes_len*cubes_len; ++i) {
			if(in.press_attract || in.press_detract) {
				float d = distance(rs.cubes[i].origin, rs.player.origin);
				if(d > cube_max_radius || (in.press_attract && d < cube_min_radius)) continue;
				cube_rigids[i]->applyCentralImpulse(tobt(
					(in.press_attract ? cube_attract : -cube_detract)*normalize(rs.player.origin - rs.cubes[i].origin)
				));
			}
		}
	}
	
	void integrate(float dts) {
		timestamp += dts;
		dworld->stepSimulation(dts, 1, dts);
	}
		
	renderstate_t to_renderstate() {
		renderstate_t rs;
		rs.timestamp = timestamp;
		btTransform xform;
		player_mstate->getWorldTransform(xform);
		rs.player.origin = toglm(xform.getOrigin());
		rs.player.orientation = toglm(xform.getRotation());
		for(size_t i = 0; i < LENGTH(cube_mstates); ++i) {
			cube_mstates[i]->getWorldTransform(xform);
			entity_t cube;
			cube.origin = toglm(xform.getOrigin());
			cube.orientation = toglm(xform.getRotation());
			rs.cubes[i] = cube;
		}
		return rs;
	}
};

struct cl_packet_t {
	int last_received_packet = 0;
	simstate_t::input_t input;
	
	SERIALIZE_OBJECT() {
		if(!serialize_compressed_int(stream, last_received_packet, 0, sv::sv_buffer - 1)) return false;
		if(!serialize_bool(stream, input.press_move_forward)) return false;
		if(!serialize_bool(stream, input.press_move_backward)) return false;
		if(!serialize_bool(stream, input.press_move_left)) return false;
		if(!serialize_bool(stream, input.press_move_right)) return false;
		if(!serialize_bool(stream, input.press_attract)) return false;
		if(!serialize_bool(stream, input.press_detract)) return false;
		// todo: compress forward, right using normalization tricks
		if(!serialize_compressed_float(stream, input.forward.x, -1.0f, 1.0f, 0.1f)) return false;
		if(!serialize_compressed_float(stream, input.forward.y, -1.0f, 1.0f, 0.1f)) return false;
		if(!serialize_compressed_float(stream, input.forward.z, -1.0f, 1.0f, 0.1f)) return false;
		if(!serialize_compressed_float(stream, input.right.x, -1.0f, 1.0f, 0.1f)) return false;
		if(!serialize_compressed_float(stream, input.right.y, -1.0f, 1.0f, 0.1f)) return false;
		if(!serialize_compressed_float(stream, input.right.z, -1.0f, 1.0f, 0.1f)) return false;
		return true;
	}
	
	packet_t to_packet() {
		packet_t packet;
		packet.data.resize(2*sizeof(cl_packet_t));
		lwrite_stream_t ws(packet.data.data(), packet.data.size());
		lassert(serialize_object(ws, *this)); ws.flush();
		packet.data.resize(ws.words_written()*4);
		return packet;
	}
	
	bool set_from_packet(const packet_t& p) {
		lread_stream_t rs(p.data.data(), p.data.size());
		return serialize_object(rs, *this);
	}
};

const char* sv_ip = "127.0.0.1";
const int sv_port = 12344;
namespace sv {
	bool sv_running = true;
	simstate_t sv_simstate;
	pthread_mutex_t sv_renderstate_mutex;
	pthread_mutex_t sv_simstate_mutex;
	int sv_rsbuffer_i = 0;
	renderstate_t sv_rsbuffer[sv_buffer];
	float sv_bps = 0.0f;
}
void* sv_thread(void* user_data) {
	using namespace sv;
	UNUSED(user_data);
	
	lassert(pthread_mutex_init(&sv_renderstate_mutex, NULL) == 0);
	DEFER(pthread_mutex_destroy(&sv_renderstate_mutex));
	lassert(pthread_mutex_init(&sv_simstate_mutex, NULL) == 0);
	DEFER(pthread_mutex_destroy(&sv_simstate_mutex));	
	
	ltime_set(ltime());
	
	ENetAddress address;
	address.host = ENET_HOST_ANY;
	address.port = sv_port;
	ENetHost* sv_host = enet_host_create(&address, 32, 2, 0, 0);
	lassert(sv_host);
	
	sv_rsbuffer_i = 0;
	pthread_mutex_lock(&sv_renderstate_mutex);
	sv_rsbuffer[sv_rsbuffer_i] = sv_simstate.to_renderstate();
	pthread_mutex_unlock(&sv_renderstate_mutex);
	for(size_t i = 1; i < sv_buffer; ++i) {
		sv_rsbuffer[i] = sv_rsbuffer[0];
	}
	
	vector<float> packet_queue_wait;
	vector<packet_t> packet_queue;
	
	float bps_sum = 0.0f;
	float bps_accumulator_s = 0.0f;
	
	int last_clp_id = 0;
	
	float sim_accumulator_s = 0.0f;
	float netcheck_accumulator_s = 0.0f;
	float last_time_s = ltime();
	while(sv_running) {
		float now_time_s = (float)ltime();
		float dts = now_time_s - last_time_s;
		DEFER(last_time_s = now_time_s);
		
		// sim, see http://gafferongames.com/game-physics/fix-your-timestep/
		sim_accumulator_s += dts;
		if(sim_accumulator_s >= sv_simrate_s) {
			pthread_mutex_lock(&sv_simstate_mutex);
			// todo: bound to an upper bound to prevent infinite loops
			while(sim_accumulator_s >= sv_simrate_s) {
				sim_accumulator_s -= sv_simrate_s;
				sv_simstate.integrate(sv_simrate_s);
			}
			pthread_mutex_lock(&sv_renderstate_mutex);
			sv_rsbuffer_i = (sv_rsbuffer_i + 1)%sv_buffer;
			sv_rsbuffer[sv_rsbuffer_i] = sv_simstate.to_renderstate();
			if(lrandf() > sv_fake_loss) {
				packet_t packet_to_send = sv_rsbuffer[sv_rsbuffer_i].to_packet(
					sv_rsbuffer_i,
					sv_rsbuffer,
					sv::sv_buffer,
					last_clp_id
				);
				
				packet_queue.push_back(packet_to_send);
				packet_queue_wait.emplace_back((float)ltime() + sv_fake_tt_ms/1000.0f);
			}
			pthread_mutex_unlock(&sv_renderstate_mutex);
			pthread_mutex_unlock(&sv_simstate_mutex);
		}
		
		netcheck_accumulator_s += dts;
		if(accum_do(&netcheck_accumulator_s, sv_netcheckrate_s)) {
			ENetEvent event;
			while(enet_host_service(sv_host, &event, 0) > 0) {
				switch(event.type) {
					case ENET_EVENT_TYPE_CONNECT: {
//						printf("(server) client %x connected.\n", event.peer->address.host);
					} break;
					case ENET_EVENT_TYPE_RECEIVE: {
						packet_t incoming;
						incoming.data.reserve(event.packet->dataLength);
						for(size_t i = 0; i < event.packet->dataLength; ++i) {
							incoming.data.push_back(event.packet->data[i]);
						}
						cl_packet_t clp;
						if(clp.set_from_packet(incoming)) {
							sv_simstate.process_input(sv_simstate.to_renderstate(), clp.input);
							last_clp_id = clp.last_received_packet;
						}
						enet_packet_destroy(event.packet);
					} break;
					case ENET_EVENT_TYPE_DISCONNECT: {
//						printf("(server) client %x disconnected.\n", event.peer->address.host);
					} break;
					default: break;
				}
			}
		}
		
		while(packet_queue_wait.size() && packet_queue_wait[0] < ltime()) {
			if(packet_queue.size() > 1 && lrandf() < sv_fake_jitter) {
				int swap_index = rand()%(lmin(5, (int)packet_queue_wait.size()));
				lswap(packet_queue[0], packet_queue[swap_index]);
				lswap(packet_queue_wait[0], packet_queue_wait[swap_index]);
			}
			packet_t packet_to_send = packet_queue[0];
				
			bps_sum += packet_to_send.data.size()*4.0f;
			packet_queue.erase(packet_queue.begin());
			packet_queue_wait.erase(packet_queue_wait.begin());
			
			ENetPacket* packet = enet_packet_create(packet_to_send.data.data(), packet_to_send.data.size(), 0);
			enet_host_broadcast(sv_host, 0, packet);
			enet_host_flush(sv_host);
		}
		
		bps_accumulator_s += dts;
		if(accum_do(&bps_accumulator_s, 1.0f)) {
			sv_bps = bps_sum;
			bps_sum = 0.0f;
		}
	}
	return NULL;
}

int main(const int argc, char* argv[]) {
	using namespace cl;
	
	srand(time(NULL));
	ltime_set(ltime());
	
	lassert(enet_initialize() == 0);
	DEFER(enet_deinitialize());
	
	pthread_t sv_tid;
	lassert(pthread_create(&sv_tid, NULL, sv_thread, NULL) == 0);
	printf("attempting to connect to %s:%d...\n", sv_ip, sv_port);
	usleep(15000);
	
	ENetHost* cl_host = enet_host_create(NULL, 1, 2, 57600/8, 14400/8);
	lassert(cl_host != NULL);

	ENetAddress sv_address;
	enet_address_set_host(&sv_address, sv_ip);
	sv_address.port = sv_port;
	
	ENetPeer* sv_peer = enet_host_connect(cl_host, &sv_address, 2, 0);
	lassert(sv_peer != NULL);

	glfwSetErrorCallback(glfw_error_callback);
	lassert(glfwInit());
	DEFER(glfwTerminate());
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_DEPTH_BITS, 24);
	GLFWwindow* window = glfwCreateWindow(resolution_x, resolution_y, "Hello World", NULL, NULL); lassert(window);
	DEFER(glfwDestroyWindow(window));
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, glfw_framebuffer_size_changed);
	glewExperimental = GL_TRUE;
	lassert(glewInit() == 0); {
		printf("Renderer: %s\n", glGetString(GL_RENDERER));
		printf("OpenGL version %s\n", glGetString(GL_VERSION));
		
		ImGui_ImplGlfwGL3_Init(window, true);
		DEFER(ImGui_ImplGlfwGL3_Shutdown());

		cam_t cam;
		cam.fovy = lrad(100.0f);
		cam.position = vec3(14.669825f, 25.291639f, 1.033389f);
		cam.orientation = quat(0.573527, 0.409916f, -0.577020f, -0.412412f);
		cam.calculate();

		float now_time_s = (float)ltime(); 
		float dts = 1.0/60.0f;
		float netcheck_accumulator_s = 0.0f;
		float netsend_accumulator_s = 0.0f;
		const common_uniform_direct_t common_direct_uniforms[] = {
			{ "cam_position",        GL_FLOAT_VEC3, &cam.position },
			{ "cam_right",           GL_FLOAT_VEC3, &cam.right },
			{ "cam_up",              GL_FLOAT_VEC3, &cam.up },
			{ "cam_forward",         GL_FLOAT_VEC3, &cam.forward },
			{ "cam_projection",      GL_FLOAT_MAT4, &cam.projection },
			{ "cam_view",            GL_FLOAT_MAT4, &cam.view },
			{ "cam_view_projection", GL_FLOAT_MAT4, &cam.view_projection },
			{ "cam_vp",              GL_FLOAT_MAT4, &cam.view_projection },
			{ "cam_tan_half_fovy",   GL_FLOAT,      &cam.tan_half_fovy },
			{ "cam_near",            GL_FLOAT,      &cam.near },
			{ "cam_far",             GL_FLOAT,      &cam.far },
			{ "cam_aspect",          GL_FLOAT,      &cam.aspect },
			{ "cursor_x", GL_FLOAT, &cursor_x },
			{ "cursor_y", GL_FLOAT, &cursor_y },
			{ "time", GL_FLOAT, &now_time_s },
			{ "resolution_x", GL_INT, &resolution_x },
			{ "resolution_y", GL_INT, &resolution_y },
		};
		
		reload_shaders(common_direct_uniforms, LENGTH(common_direct_uniforms));
		reload_meshes();
		reload_buffers();
		DEFER(
			for(size_t i = 0; i < LENGTH(programs); ++i) {
				programs[i]->destroy();
			}
			for(size_t i = 0; i < LENGTH(mesh_obj_pairs); ++i) {
				mesh_obj_pairs[i].mesh->destroy();
			}
			color_a.destroy();
			depth_a.destroy();
			fbo_a.destroy();
			color_b.destroy();
			depth_b.destroy();
			fbo_b.destroy();
		);

		const vec2 quad_vertices[] = {
			{ -1.0f, -1.0f, },
			{  1.0f, -1.0f, },
			{ -1.0f,  1.0f, },
			{  1.0f,  1.0f, },
		};
		vbo_t quad_vbo; quad_vbo.create(); DEFER(quad_vbo.destroy());
		quad_vbo.bind(GL_ARRAY_BUFFER);
		gl(BufferData(
			GL_ARRAY_BUFFER,
			sizeof(quad_vertices),
			quad_vertices,
			GL_STATIC_DRAW
		));

		vao_t quad_vao; quad_vao.create(); DEFER(quad_vao.destroy());
		quad_vao.bind();
		gl(EnableVertexAttribArray(0));
		gl(BindBuffer(GL_ARRAY_BUFFER, quad_vbo));
		for(int o = 0; o < (int)LENGTH(programs); ++o) {
			program_ext_t& pr = *programs[o];
			for(size_t e = 0; e < pr.data.attributes.size(); ++e) {
				program_attribute_t& at = pr.data.attributes[e];
				for(int i = 0; i < (int)LENGTH(common_attributes); ++i) {
					const common_attribute_t& ca = common_attributes[i];
					if(ca.type == at.type && ca.name == at.name) {
						gl(VertexAttribPointer(at.handle, 2, GL_FLOAT, GL_FALSE, 0, NULL));
					}
				}
			}
		}
		quad_vao.unbind();
		
		renderstate_t rs_prev, rs_alpha, rs_next; {
			simstate_t tmp; rs_alpha = tmp.to_renderstate();
			rs_prev = rs_next = rs_alpha;
		}
		int rs_db_i = 0;
		renderstate_t rs_db[sv::sv_buffer];
		for(size_t i = 0; i < sv::sv_buffer; ++i) {
			rs_db[i] = rs_alpha;
		}
		jitter_t<client_jitter> jitter;
		jitter.add(rs_alpha);
		
		bool in_locked_cam = true;
		bool in_mouse_look = false;
		
		float last_time_s = (float)ltime();
		ImGui::SetNextWindowCollapsed(true);
		do {
			// events
			glfwPollEvents();
			ImGui_ImplGlfwGL3_NewFrame();
			
			// delta time
			now_time_s = (float)ltime();
			dts = now_time_s - last_time_s;
			DEFER(last_time_s = now_time_s);
				
			// cursor
			middle_x = (float)resolution_x/2.0f, middle_y = (float)resolution_y/2.0f;
			{
				double dcursor_x, dcursor_y;
				glfwGetCursorPos(window, &dcursor_x, &dcursor_y);
				cursor_x = dcursor_x; cursor_y = dcursor_y;
			}
			
			// keyboard input
			bool press_reload      = glfwGetKey(window, GLFW_KEY_R);
			bool press_esc         = glfwGetKey(window, GLFW_KEY_ESCAPE);
			simstate_t::input_t sim_input;
			sim_input.press_move_forward     = glfwGetKey(window, GLFW_KEY_I);
			sim_input.press_move_backward    = glfwGetKey(window, GLFW_KEY_K);
			sim_input.press_move_left        = glfwGetKey(window, GLFW_KEY_J);
			sim_input.press_move_right       = glfwGetKey(window, GLFW_KEY_L);
			sim_input.press_attract          = glfwGetKey(window, GLFW_KEY_U);
			sim_input.press_detract          = glfwGetKey(window, GLFW_KEY_O);
			cam_t::input_t cam_input;
			cam_input.press_forward     = glfwGetKey(window, GLFW_KEY_W);
			cam_input.press_backward    = glfwGetKey(window, GLFW_KEY_S);
			cam_input.press_left        = glfwGetKey(window, GLFW_KEY_A);
			cam_input.press_right       = glfwGetKey(window, GLFW_KEY_D);
			cam_input.press_local_up    = glfwGetKey(window, GLFW_KEY_SPACE);
			cam_input.press_local_down  = glfwGetKey(window, GLFW_KEY_C);
			cam_input.press_global_up   = glfwGetKey(window, GLFW_KEY_X);
			cam_input.press_global_down = glfwGetKey(window, GLFW_KEY_Z);
			cam_input.press_roll_left   = glfwGetKey(window, GLFW_KEY_Q);
			cam_input.press_roll_right  = glfwGetKey(window, GLFW_KEY_E);

			fbo_a.bind();
#if RENDER_WHAT == RENDER_BOTH
			gl(Viewport(0, 0, resolution_x/2, resolution_y));
#else
			gl(Viewport(0, 0, resolution_x, resolution_y));
#endif
			gl(Disable(GL_CULL_FACE));
			gl(Enable(GL_DEPTH_TEST));
			gl(Enable(GL_BLEND)); gl(BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)); 
			gl(ClearColor(0.1f, 0.2f, 0.3f, 1.0f));
			gl(Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
			
			// rs_alpha here
			rs_alpha.lerp(rs_alpha, rs_next, cl_smooth);
			if(in_mouse_look) { cam.process_mouselook(dts, window); }
			cam.process_movement(dts, window, cam_input);
			{
				static vec3 last_cam_positions[3];
				static quat last_cam_orientations[3];
				static bool first = true;
				if(first) {
					first = false;
					for(size_t z = 0; z < LENGTH(last_cam_orientations); ++z) {
						last_cam_positions[z] = cam.position;
						last_cam_orientations[z] = cam.orientation;
					}
				} 
				if(in_locked_cam) {
					cam.position = rs_alpha.player.origin + vec3(0.0f, 0.0f, cam_depth);
					cam.position.y = cam_depth;
					cam.orientation = quat(lookAt(cam.position, rs_alpha.player.origin, axis_y));			
				}
				if(!first) {
					vec3 cam_pos_accum = cam.position;
					quat cam_orientation_accum = cam.orientation;
					for(size_t z = 0; z < LENGTH(last_cam_orientations); ++z) {
						cam_pos_accum += last_cam_positions[z];
						cam_orientation_accum.x += last_cam_orientations[z].x;
						cam_orientation_accum.y += last_cam_orientations[z].y;
						cam_orientation_accum.z += last_cam_orientations[z].z;
						cam_orientation_accum.w += last_cam_orientations[z].w;
					}
					cam_pos_accum /= (LENGTH(last_cam_positions) + 1);
					cam_orientation_accum.x /= (LENGTH(last_cam_positions) + 1);
					cam_orientation_accum.y /= (LENGTH(last_cam_positions) + 1);
					cam_orientation_accum.z /= (LENGTH(last_cam_positions) + 1);
					cam_orientation_accum.w /= (LENGTH(last_cam_positions) + 1);
					cam.position = cam_pos_accum;
					cam.orientation = cam_orientation_accum;
					for(size_t z = 1; z < LENGTH(last_cam_orientations); ++z) {
						last_cam_orientations[z - 1] = last_cam_orientations[z];
						last_cam_positions[z - 1] = last_cam_positions[z];
					}
					last_cam_positions[LENGTH(last_cam_orientations) - 1] = cam.position;
					last_cam_orientations[LENGTH(last_cam_orientations) - 1] = cam.orientation;
				}
			}
#if RENDER_WHAT == RENDER_BOTH
			cam.aspect = ((float)resolution_x/2.0f)/(float)resolution_y;
#else
			cam.aspect = ((float)resolution_x)/(float)resolution_y;
#endif
			cam.calculate();
			rs_alpha.render(cam.view_projection);
			
			
#if RENDER_WHAT == RENDER_BOTH
			fbo_b.bind();
			gl(Viewport(0, 0, resolution_x/2, resolution_y));
			gl(Disable(GL_CULL_FACE));
			gl(Enable(GL_DEPTH_TEST));
			gl(Enable(GL_BLEND)); gl(BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)); 
			gl(ClearColor(0.1f, 0.2f, 0.3f, 1.0f));
			gl(Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
			
			pthread_mutex_lock(&sv::sv_renderstate_mutex);
			sv::sv_rsbuffer[sv::sv_rsbuffer_i].render(cam.view_projection);
			pthread_mutex_unlock(&sv::sv_renderstate_mutex);
#endif
			
			
			fbo_t::unbind();
			gl(Viewport(0, 0, resolution_x, resolution_y));
			gl(Disable(GL_CULL_FACE));
			gl(Disable(GL_DEPTH_TEST));
			gl(Enable(GL_BLEND)); gl(BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)); 
			gl(ClearColor(0.0f, 1.0f, 0.0f, 1.0f));
			gl(Clear(GL_COLOR_BUFFER_BIT));
			
			// postfx (to screen)
			quad_vao.bind();
			postfx.bind();
			postfx.data.set_uniforms();
			if(postfx_left != IHANDLE) gl(Uniform1i(postfx_left, color_a.bind(0)));
			if(postfx_right != IHANDLE) gl(Uniform1i(postfx_right, color_b.bind(1)));
			gl(DrawArrays(GL_TRIANGLE_STRIP, 0, LENGTH(quad_vertices)));
			color_b.unbind(1);
			color_a.unbind(0);
			postfx.unbind();
			quad_vao.unbind();
			
			{ // gui
#define RATE_HZ(A) \
				static float A##_hz = 1.0f/A; \
				ImGui::SliderFloat(#A "_hz", &A##_hz, 90.0f, 1.0f, "%.2f"); \
				A = 1.0f/A##_hz;
				
				using namespace sv;
				ImGui::Text("%.3f kbps\n", sv_bps/1000.0f);
				ImGui::SliderFloat("sv_fake_tt_ms", &sv_fake_tt_ms, 0.0f, 300.0f, "%.1f");
				ImGui::SliderFloat("sv_fake_loss", &sv_fake_loss, 0.0f, 1.0f, "%.2f");
				ImGui::SliderFloat("sv_fake_jitter", &sv_fake_jitter, 0.0f, 1.0f, "%.2f");
				
				RATE_HZ(sv_netcheckrate_s);
				RATE_HZ(sv_simrate_s);			
				
				RATE_HZ(cl_netsendrate_s);
				RATE_HZ(cl_netcheckrate_s);
				ImGui::SliderFloat("cl_smooth", &cl_smooth, 0.0f, 1.0f, "%.2f");
				
				if(ImGui::Button("Locked Cam")) {
					in_locked_cam = !in_locked_cam;
				}
				if(ImGui::Button("reset")) {
					pthread_mutex_lock(&sv::sv_simstate_mutex);
					sv::sv_simstate.init();
					pthread_mutex_unlock(&sv::sv_simstate_mutex);
				}
				
#if 0
				if(ImGui::Button("reset")) {
					sv::sv_simstate.init();
					sv::sv_renderstate_alpha = 0.0f;
					sv::sv_renderstate_curr = sv::sv_simstate.to_renderstate();
					sv::sv_simstate.integrate(sv_simrate_s);
					sv::sv_renderstate_next = sv::sv_simstate.to_renderstate();
					rs_client = sv::sv_simstate.to_renderstate();
				}
#endif
			}
			
			ImGui::Render();
			glfwSwapBuffers(window);
			
			sim_input.forward = cam.forward;
			sim_input.right = cam.right;

			netcheck_accumulator_s += dts;
			if(accum_do(&netcheck_accumulator_s, cl_netcheckrate_s)) {
				ENetEvent event;
				while(enet_host_service(cl_host, &event, 0) > 0) {
					switch(event.type) {
						case ENET_EVENT_TYPE_CONNECT: {
//							printf("(client) connected to server %x\n", event.peer->address.host);
						} break;
						case ENET_EVENT_TYPE_RECEIVE: {
							DEFER(enet_packet_destroy(event.packet));
							// todo: use memcpy
							packet_t incoming;
							incoming.data.reserve(event.packet->dataLength);
							for(size_t i = 0; i < event.packet->dataLength; ++i) {
								incoming.data.push_back(event.packet->data[i]);
							}
							renderstate_t rs_new;
							int new_rs_id;
							if(rs_new.set_from_packet(incoming, rs_db, sv::sv_buffer, &new_rs_id)) {
								rs_db[new_rs_id] = rs_new;
								rs_db_i = new_rs_id;
								
								if(jitter.add(rs_new, rs_alpha.timestamp, &rs_next)) {
									rs_prev = rs_alpha;
								}
							}
						} break;
						case ENET_EVENT_TYPE_DISCONNECT: {
//							printf("(client) server %x disconnected.\n", event.peer->address.host);
						} break;
						default: break;
					}
				}
			}

			netsend_accumulator_s += dts;
			if(accum_do(&netsend_accumulator_s, cl_netsendrate_s)) {
				cl_packet_t packet_to_send;
				packet_to_send.input = sim_input;
				packet_to_send.last_received_packet = rs_db_i;
				packet_t p = packet_to_send.to_packet();
				ENetPacket* packet = enet_packet_create(p.data.data(), p.data.size(), 0);
				enet_peer_send(sv_peer, 0, packet);
				enet_host_flush(cl_host);
			}
			
			{ // reload files
				static bool last_reload = false;
				if(press_reload && !last_reload) {
					reload_shaders(common_direct_uniforms, LENGTH(common_direct_uniforms));
				}
				last_reload = press_reload;
			}
			
			{ // toggle in_mouse_look
				static double start_cursor_x = cursor_x, start_cursor_y = cursor_y;
				static bool last_esc = false;
				DEFER(last_esc = press_esc);
				if(press_esc && !last_esc) {
					in_mouse_look = !in_mouse_look;
					if(in_mouse_look) {
						start_cursor_x = cursor_x; start_cursor_y = cursor_y;
						glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
						glfwSetCursorPos(window, cursor_x = middle_x, cursor_y = middle_y);
					} else {
						glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
						glfwSetCursorPos(window, start_cursor_x, start_cursor_y);
					}
				}
			}	
		} while(!glfwWindowShouldClose(window));
	}
	
#if 0
	{
		printf("(client) disconnecting from %s:%d\n", sv_ip, sv_port);
		enet_peer_disconnect(sv_peer, 0);
		ENetEvent event;
		while(enet_host_service(cl_host, &event, 1000) > 0) {
			switch(event.type) {
				case ENET_EVENT_TYPE_RECEIVE: { enet_packet_destroy(event.packet); } break;
				default: break;
			}
		}
	}
#endif
	
	sv::sv_running = false;
	pthread_join(sv_tid, NULL);
	return 0;
}
