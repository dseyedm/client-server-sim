#pragma once
#include "lserialize_old.h"
#include "lserialize_attempt.h"

// note:
// special thanks to Glenn Fiedler for his excellent series on game networking
// see @ http://gafferongames.com/2015/03/14/the-networked-physics-data-compression-challenge/

#include "common.h"

struct lbit_writer_t {
	u32* const out_data = NULL;
	const int out_data_bytes = 0;
	const int out_data_bits = 0;
	const int out_data_words = 0;
	int out_data_index = 0;
	int bits_written = 0;
	u64 bitbuffer = 0;
	int bit_index = 0;
	
	lbit_writer_t(u32* d, int bytes) 
	: out_data(d), 
	  out_data_bytes(bytes),
	  out_data_bits(bytes*8),
	  out_data_words(bytes/4) {
		lassert(bytes%4 == 0);
	}
	
	bool write_bits(u32 value, int value_bits) {
		lassert(0 < value_bits);
		lassert(value_bits <= 32);
		lassert(bits_written + value_bits <= out_data_bits);
		if(bits_written + value_bits > out_data_bits) { return false; }
		bits_written += value_bits;
		
		// zero out any garbage bits in value.
		value &= ((u64)(1) << value_bits) - 1;
		// place value in the leftmost (highest) range in bitbuffer.
		bitbuffer |= (u64)(value) << (64 - bit_index - value_bits);
		bit_index += value_bits;
		// flush if there is overflow.
		if(bit_index >= 32) {
			lassert(out_data_index < out_data_words);
			// flush the highest 32 bits.
			out_data[out_data_index] = lhost_to_network((u32)(bitbuffer >> 32));
			++out_data_index;
			// replace the flushed highest 32 bits with the lower section.
			bitbuffer <<= 32;
			bit_index -= 32;
		}
		return true;
	}
	
	void write_align() {
		int remainder_bits = bits_written%8;
		if(remainder_bits == 0) return;
		u32 zero = 0;
		write_bits(zero, 8 - remainder_bits);
		lassert(bits_written%8 == 0);
	}
	
	int get_align_bits() { return (8 - bits_written%8)%8; }
	
	bool write_bytes(const u8* data, int bytes) {
		lassert(get_align_bits() == 0);
		assert(bits_written + bytes*8 <= out_data_bits);
		if(bits_written + bytes*8 > out_data_bits) { return false; }
		lassert(bit_index == 8*0 || bit_index == 8*1 || bit_index == 8*2 || bit_index == 8*3);
		
		// [head] [middle : u32] [tail]
		
		// head
		int head_bytes = (4 - bit_index/8)%4;
		head_bytes = lmin(head_bytes, bytes);
		for(int i = 0; i < head_bytes; ++i) {
			lassert(write_bits(data[i], 8));
		}
		if(head_bytes == bytes) return true;
		
		// middle
		lassert(get_align_bits() == 0);
		int middle_words = (bytes - head_bytes)/4;
		if(middle_words > 0) {
			lassert(bit_index == 0);
			memcpy(&out_data[out_data_index], &data[head_bytes], middle_words*4);
			bits_written += middle_words*32;
			out_data_index += middle_words;
			bitbuffer = 0;
		}
		
		// tail
		lassert(get_align_bits() == 0);
		int tail_start = head_bytes + middle_words*4;
		int tail_bytes = bytes - tail_start;
		lassert(tail_bytes >= 0 && tail_bytes < 4);
		for(int i = 0; i < tail_bytes; ++i) {
			lassert(write_bits(data[tail_start + i], 8));
		}
		
		lassert(get_align_bits() == 0);
		lassert(head_bytes + middle_words*4 + tail_bytes == bytes);
		return true;
	}
	
	bool flush() {
		if(bit_index == 0) return true;
		lassert(out_data_index < out_data_words);
		if(out_data_index >= out_data_words) return false;
		out_data[out_data_index++] = lhost_to_network((u32)(bitbuffer >> 32));
		return true;
	}
};

struct lbit_reader_t {
	const u32* in_data;
	const int in_data_bytes;
	const int in_data_bits;
	const int in_data_words;
	int in_data_index = 0;
	int bits_read = 0;
	u64 bitbuffer = 0;
	int bit_index = 0;
	lbit_reader_t(const u32* data, int bytes)
	: in_data(data),
	  in_data_bytes(bytes),
	  in_data_bits(bytes*8),
	  in_data_words(bytes/4) { 
		  lassert(data);
		  lassert(bytes > 0);
		  lassert(bytes%4 == 0);
		  bitbuffer = lnetwork_to_host(data[0]);
	}
	
	bool read_bits(u32* out_value, int value_bits) {
		lassert(value_bits > 0);
		lassert(value_bits <= 32);
		lassert(bits_read + value_bits <= in_data_bits);
		if(bits_read + value_bits > in_data_bits) { return false; }
		bits_read += value_bits;
		
		lassert(bit_index < 32);
		if(bit_index + value_bits < 32) {
			// 0000 abcd
			// 000a bcd0
			// 0000 bcd0
			bitbuffer <<= value_bits;
			bit_index += value_bits;
		} else {
			// 0000 x000 (bit_index = 3)
			// 000x 0000
			// 000x xyzz
			// 00xx yyz0
			// 0000 yyz0
			++in_data_index;
			lassert(in_data_index < in_data_words);
			const u32 a = 32 - bit_index;
			const u32 b = value_bits - a;
			bitbuffer <<= a;
			bitbuffer |= lnetwork_to_host(in_data[in_data_index]);
			bitbuffer <<= b;
			bit_index = b;
		}
		*out_value = (u32)(bitbuffer >> 32);
		bitbuffer &= 0xFFFFFFFF;
		return true;
	}
	
	bool read_bytes(u8* out_data, int out_data_bytes) {
		lassert(get_align_bits() == 0);
		
		lassert(bits_read + out_data_bytes <= in_data_bits);
		if(bits_read + out_data_bytes*8 > in_data_bits) { return false; }
		
		lassert(bit_index == 8*0 || bit_index == 8*1 || bit_index == 8*2 || bit_index == 8*3);
		
		int head_bytes = (4 - bit_index/8)%4;
		head_bytes = lmin(head_bytes, out_data_bytes);
		for(int i = 0; i < head_bytes; ++i) {
			u32 u32_value;
			lassert(read_bits(&u32_value, 8));
			out_data[i] = u32_value;
		}
		if(head_bytes == out_data_bytes) { return true; }
		
		lassert(get_align_bits() == 0);
		int middle_words = (out_data_bytes - head_bytes)/4;
		if(middle_words > 0) {
			lassert(bit_index == 0);
			memcpy(&out_data[head_bytes], &in_data[in_data_index], middle_words*4);
			bits_read += middle_words*32;
			in_data_index += middle_words;
			bitbuffer = lnetwork_to_host(in_data[in_data_index]);
		}
		
		lassert(get_align_bits() == 0);
		int tail_start = head_bytes + middle_words*4;
		int tail_bytes = out_data_bytes - tail_start;
		lassert(0 <= tail_bytes && tail_bytes < 4);
		for(int i = 0; i < tail_bytes; ++i) {
			u32 u32_value;
			lassert(read_bits(&u32_value, 8));
			out_data[tail_start + i] = u32_value;
		}
		
		lassert(get_align_bits() == 0);
		lassert(head_bytes + middle_words*4 + tail_bytes == out_data_bytes);
		return true;
	}
	
	int get_align_bits() { return (8 - bits_read%8)%8; }
	
	bool read_align() {
		int remainder_bits = bits_read%8;
		if(remainder_bits == 0) return true;
		u32 u32_value;
		if(!read_bits(&u32_value, 8 - remainder_bits)) return false;
		if(u32_value != 0) return false;
		lassert(bits_read%8 == 0);
		return true;
	}
};

struct lwrite_stream_t {
	enum { is_writing = true, is_reading = false };
	
	lbit_writer_t writer;
	
	lwrite_stream_t(u8* out_data, int out_data_bytes) : writer((u32*)out_data, out_data_bytes) { }
	
	bool stream_int(i32 value, i32 value_min, i32 value_max) {
		lassert(value_min < value_max);
		lassert(value >= value_min);
		lassert(value <= value_max);
		u32 u32_value = (u32)(value - value_min);
		return writer.write_bits(u32_value, lbits_required(value_min, value_max));
	}
	bool stream_bits(u32 value, int bits) {
		return writer.write_bits(value, bits);
	}
	bool stream_bytes(const u8* data, int bytes) {
		writer.write_align();
		return writer.write_bytes(data, bytes);
	}
	bool check(u32 magic) {
		writer.write_align();
		return writer.write_bits(magic, 32);
	}
	bool align() {
		writer.write_align();
		return true;
	}
	bool flush() {
		writer.flush();
		return true;
	}
	
	int bytes_written() { return (writer.bits_written + 7)/8; }
	int words_written() { return (writer.bits_written + 31 + 31)/32; }
};

struct lmeasure_stream_t {
	enum { is_writing = true, is_reading = false };
	
	int bits_written = 0;
	
	bool stream_int(i32 value, i32 value_min, i32 value_max) {
		lassert(value_min < value_max);
		lassert(value >= value_min);
		lassert(value <= value_max);
		bits_written += lbits_required(value_min, value_max);
		return true;
	}
	bool stream_bits(u32 value, int bits) {
		UNUSED(value);
		bits_written += bits;
		return true;
	}
	bool stream_bytes(const u8* data, int bytes) {
		UNUSED(data);
		align();
		bits_written += bytes*8;
		return true;
	}
	bool check(u32 magic) {
		UNUSED(magic);
		align();
		bits_written += 32;
		return true;
	}
	bool align() { 
		bits_written += get_align_bits(); 
		return true;
	}
	int get_align_bits() { return (8 - bits_written%8)%8; }
};

struct lread_stream_t {
	enum { is_writing = false, is_reading = true };
	
	lbit_reader_t reader;
	
	lread_stream_t(const u8* in_data, int in_data_bytes) : reader((const u32*)in_data, in_data_bytes) { }
	
	bool stream_int(i32& out_value, i32 value_min, i32 value_max) {
		lassert(value_min < value_max);
		const int bits = lbits_required(value_min, value_max);
		u32 u32_value;
		if(!reader.read_bits(&u32_value, bits)) { return false; }
		out_value = (i32)(u32_value) + value_min;
		return true;
	}
	bool stream_bits(u32& out_value, int bits) {
		return reader.read_bits(&out_value, bits);
	}
	bool stream_bytes(u8* out_data, int bytes) {
		if(!reader.read_align()) return false;
		return reader.read_bytes(out_data, bytes);
	}
	bool check(u32 magic) {
		if(!reader.read_align()) return false;
		u32 u32_value;
		if(!reader.read_bits(&u32_value, 32)) return false;
		return magic == u32_value;
	}
	bool align() {
		return reader.read_align();
	}
};

template<typename T> bool serialize_object(lread_stream_t& stream, T& object, void* user_ptr = NULL) { return object.serialize_read(stream, user_ptr); }
template<typename T> bool serialize_object(lwrite_stream_t& stream, T& object, void* user_ptr = NULL) { return object.serialize_write(stream, user_ptr); }
template<typename T> bool serialize_object(lmeasure_stream_t& stream, T& object, void* user_ptr = NULL) { return object.serialize_measure(stream, user_ptr); }

#define SERIALIZE_OBJECT() \
	bool serialize_read(struct lread_stream_t& stream, void* user_ptr = NULL) { return serialize(stream, user_ptr); }; \
	bool serialize_write(struct lwrite_stream_t& stream, void* user_ptr = NULL) { return serialize(stream, user_ptr); }; \
	bool serialize_measure(struct lmeasure_stream_t& stream, void* user_ptr = NULL) { return serialize(stream, user_ptr); }; \
	template<typename stream_type> bool serialize(stream_type& stream, void* user_ptr = NULL)

#define _serialize_bits(stream, value, bits) { \
	u32 u32_value; \
	if(stream_type::is_writing) { \
		u32_value = (u32)value; \
	} \
	if(!stream.stream_bits(u32_value, bits)) { return false; } \
	if(stream_type::is_reading) { \
		value = (decltype(value))u32_value; \
	} \
}

template<typename stream_type> bool serialize_bool(stream_type& stream, bool& value) { _serialize_bits(stream, value, 1); return true; }
template<typename stream_type> bool serialize_u16(stream_type& stream, u16& value) { _serialize_bits(stream, value, 16); return true; }
template<typename stream_type> bool serialize_u32(stream_type& stream, u32& value) { _serialize_bits(stream, value, 32); return true; }
template<typename stream_type> bool serialize_u64(stream_type& stream, u64& value) {
	u32 hi,lo;
	if(stream_type::is_writing) {
		lo = value & 0xFFFFFFFF;
		hi = value >> 32;
	}
	_serialize_bits(stream, lo, 32);
	_serialize_bits(stream, hi, 32);
	if(stream_type::is_reading) {
		value = (u64(hi) << 32) | lo;
	}
	return true;
}

template<typename stream_type> bool serialize_i16(stream_type& stream, i16& value) { _serialize_bits(stream, value, 16); return true; }
template<typename stream_type> bool serialize_i32(stream_type& stream, i32& value) { _serialize_bits(stream, value, 32); return true; }
template<typename stream_type> bool serialize_i64(stream_type& stream, i64& value) {
	u32 hi,lo;
	if(stream_type::is_writing) {
		lo = u64(value) & 0xFFFFFFFF;
		hi = u64(value) >> 32;
	}
	_serialize_bits(stream, lo, 32);
	_serialize_bits(stream, hi, 32);
	if(stream_type::is_reading) {
		value = (i64(hi) << 32) | lo;
	}
	return true;
}
template<typename stream_type> bool serialize_compressed_int(stream_type& stream, i32& value, i32 value_min, i32 value_max) {
	i32 i32_value;
	if(stream_type::is_writing) {
		i32_value = value;
	}
	if(!stream.stream_int(i32_value, value_min, value_max)) { return false; }
	if(stream_type::is_reading) {
		value = (decltype(value))i32_value;
	}
	return true;
}

template<typename stream_type> bool serialize_float(stream_type& stream, float& value) {
	union float_int_u {
		float float_value;
		u32 int_value;
	};
	float_int_u tmp;
	if(stream_type::is_writing) {
		tmp.float_value = value;
	}
	if(!serialize_u32(stream, tmp.int_value)) return false;
	if(stream_type::is_reading) {
		value = tmp.float_value;
	}
	return true;
}

template<typename stream_type> inline bool serialize_compressed_float(stream_type& stream, float& value, float min, float max, float res) {
	const float delta = max - min;
	const float values = delta/res;
	const u32 max_int_value = (u32)ceil(values);
	const int bits = lbits_required(0, max_int_value);
	u32 int_value = 0;
	if(stream_type::is_writing) {
		float normalized_value = lclamp(0.0f, (value - min)/delta, 1.0f);
		int_value = (u32)floor(normalized_value*max_int_value + 0.5f);
	}
	if(!stream.stream_bits(int_value, bits)) return false;
	if(stream_type::is_reading) {
		const float normalized_value = int_value/float(max_int_value);
		value = normalized_value*delta + min;
	}
	return true;
}

template<typename stream_type> bool serialize_double(stream_type& stream, double& value) {
	union double_int_u {
		double double_value;
		u64 int_value;
	};
	double_int_u tmp;
	if(stream_type::is_writing) {
		tmp.double_value = value;
	}
	if(!serialize_u64(stream, tmp.int_value)) return false;
	if(stream_type::is_reading) {
		value = tmp.double_value;
	}
	return true;
}

template<typename stream_type> bool serialize_bytes(stream_type& stream, u8* data, int bytes) { stream.stream_bytes(data, bytes); return true; }
template<typename stream_type> bool serialize_string(stream_type& stream, char* string, int buffer_size) {
	u32 length;
	if(stream_type::is_writing) {
		length = strlen(string);
	}
	if(!stream.align()) return false;
	if(!stream.stream_bits(length, 32)) return false;
	lassert(length < buffer_size - 1);
	stream.stream_bytes((u8*)string, length);
	if(stream_type::is_reading) {
		string[length] = '\0';
	}
	return true;
}

template<typename stream_type> bool serialize_check(stream_type& stream, u32 magic) {
	if(!stream.check(magic)) return false;
	return true;
}

template<int bits> struct lcompressed_quat_t {
	enum { max_value = (1 << bits) - 1 };

	u32 largest : 2;
	u32 integer_a : bits;
	u32 integer_b : bits;
	u32 integer_c : bits;

	void compress(float x, float y, float z, float w) {
		lassert(bits > 1);
		lassert(bits <= 10);

		const float minimum = -1.0f / 1.414214f;       // 1.0f / sqrt(2)
		const float maximum = +1.0f / 1.414214f;

		const float scale = float((1 << bits) - 1);

		const float abs_x = fabs(x);
		const float abs_y = fabs(y);
		const float abs_z = fabs(z);
		const float abs_w = fabs(w);

		largest = 0;
		float largest_value = abs_x;

		if(abs_y > largest_value) {
			largest = 1;
			largest_value = abs_y;
		}

		if(abs_z > largest_value) {
			largest = 2;
			largest_value = abs_z;
		}

		if(abs_w > largest_value) {
			largest = 3;
			largest_value = abs_w;
		}

		float a = 0;
		float b = 0;
		float c = 0;

		switch(largest) {
			case 0:
			if(x >= 0) {
				a = y;
				b = z;
				c = w;
			} else {
				a = -y;
				b = -z;
				c = -w;
			} break;

			case 1:
			if(y >= 0) {
				a = x;
				b = z;
				c = w;
			} else {
				a = -x;
				b = -z;
				c = -w;
			} break;

			case 2:
			if(z >= 0) {
				a = x;
				b = y;
				c = w;
			} else {
				a = -x;
				b = -y;
				c = -w;
			} break;

			case 3:
			if(w >= 0) {
				a = x;
				b = y;
				c = z;
			} else {
				a = -x;
				b = -y;
				c = -z;
			} break;

			default: lassert(false);
		}

		const float normal_a = (a - minimum) / (maximum - minimum); 
		const float normal_b = (b - minimum) / (maximum - minimum);
		const float normal_c = (c - minimum) / (maximum - minimum);

		integer_a = floor(normal_a*scale + 0.5f);
		integer_b = floor(normal_b*scale + 0.5f);
		integer_c = floor(normal_c*scale + 0.5f);
	}

	// note: you're going to want to normalize the quaternion returned from this function
	void uncompress(float* out_x, float* out_y, float* out_z, float* out_w) const {
		float& x = *out_x; 
		float& y = *out_y; 
		float& z = *out_z; 
		float& w = *out_w;
		lassert(bits > 1);
		lassert(bits <= 10);

		const float minimum = - 1.0f / 1.414214f;       // 1.0f / sqrt(2)
		const float maximum = + 1.0f / 1.414214f;

		const float scale = float((1 << bits) - 1);

		const float inverse_scale = 1.0f / scale;

		const float a = integer_a * inverse_scale * (maximum - minimum) + minimum;
		const float b = integer_b * inverse_scale * (maximum - minimum) + minimum;
		const float c = integer_c * inverse_scale * (maximum - minimum) + minimum;

		switch(largest) {
			case 0: {
				x = sqrtf(1 - a*a - b*b - c*c);
				y = a;
				z = b;
				w = c;
			} break;

			case 1: {
				x = a;
				y = sqrtf(1 - a*a - b*b - c*c);
				z = b;
				w = c;
			} break;

			case 2: {
				x = a;
				y = b;
				z = sqrtf(1 - a*a - b*b - c*c);
				w = c;
			} break;

			case 3: {
				x = a;
				y = b;
				z = c;
				w = sqrtf(1 - a*a - b*b - c*c);
			} break;

			default: {
				lassert(false);
				x = 0;
				y = 0;
				z = 0;
				w = 1;
			}
		}
	}

	SERIALIZE_OBJECT() {
		_serialize_bits(stream, largest, 2);
		_serialize_bits(stream, integer_a, bits);
		_serialize_bits(stream, integer_b, bits);
		_serialize_bits(stream, integer_c, bits);
		return true;
	}

	bool operator == (const lcompressed_quat_t & other) const {
		if(largest != other.largest) return false;
		if(integer_a != other.integer_a) return false;
		if(integer_b != other.integer_b) return false;
		if(integer_c != other.integer_c) return false;
		return true;
	}

	bool operator != (const lcompressed_quat_t & other) const { return !(*this == other); }
};
