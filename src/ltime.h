#pragma once

#include <stdint.h>

uint64_t ltime_freq();
uint64_t ltime_value();

extern uint64_t ltime_offset;
inline double ltime() { return (double)(ltime_value() - ltime_offset)/ltime_freq(); }
inline void ltime_set(double time) { ltime_offset = ltime_value() - (uint64_t)(time*ltime_freq()); }

#if defined(__APPLE__)
#include <mach/mach_time.h>
inline uint64_t ltime_freq() { 
	mach_timebase_info_data_t info;
	mach_timebase_info(&info);
	return (info.denom*1e9)/info.numer;
}
inline uint64_t ltime_value() { return mach_absolute_time(); }

#elif defined(_WIN32)
#include <windows.h>
inline uint64_t ltime_freq() {
	uint64_t frequency;
	if(QueryPerformanceFrequency((LARGE_INTEGER*)&frequency)) return frequency;
	return 1000;
}
inline uint64_t ltime_value() {
	uint64_t value;
	if(QueryPerformanceCounter((LARGE_INTEGER*)&value)) return value;
	return (uint64_t)timeGetTime();
}

#else // posix?
#include <sys/time.h>
#include <time.h>
inline uint64_t ltime_freq() {
#if defined(CLOCK_MONOTONIC)
	struct timespec ts;
	if(clock_gettime(CLOCK_MONOTONIC, &ts) == 0) return 1000000000;
#endif
	return 1000000;
}
inline uint64_t ltime_value() {
#if defined(CLOCK_MONOTONIC)
	struct timespec ts;
	if(clock_gettime(CLOCK_MONOTONIC, &ts) == 0) return (uint64_t)ts.tv_sec*(uint64_t)1000000000 + (uint64_t)ts.tv_nsec;
#endif
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (uint64_t)tv.tv_sec*(uint64_t)1000000 + (uint64_t)tv.tv_usec;
}
#endif
