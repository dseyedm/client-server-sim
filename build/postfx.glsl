#pragma vertex
#pragma include "common.glsl"
in vec2 vertex;
out vec2 frag_uv;
void main() {
	frag_uv = vertex.xy*0.5 + 0.5;
	gl_Position = vec4(vertex.xy, 0.0, 1.0);
}

#pragma fragment
#pragma include "common.glsl"
in vec2 frag_uv;
uniform sampler2D left;
uniform sampler2D right;
out vec4 out_frag;
void main() {
#if 1
	if(frag_uv.x < 0.5) {
		out_frag = texture(left, frag_uv*vec2(2.0, 1.0)).rgba;
	} else {
		out_frag = texture(right, (frag_uv - vec2(0.5, 0.0))*vec2(2.0, 1.0)).rgba;
	}
#else
	out_frag = texture(left, frag_uv).rgba;
#endif
}
