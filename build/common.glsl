#version 330

#define PI 3.14159265
#define TAU (2*PI)

uniform float time;

float vary() { return cos(time)*0.5 + 0.5; }
float saturate(float f) { return clamp(f, 0.0, 1.0); }
vec3 encode_normal(vec3 normal) { return normal*0.5 + 0.5; }
