#pragma vertex
#pragma include "common.glsl"

uniform float cam_tan_half_fovy;
uniform float cam_aspect;
uniform mat4 model;
uniform mat4 model_view_projection;

in vec3 vertex;
in vec3 normal;
in vec2 texcoord;

smooth out vec2 f_texcoord;
smooth out vec3 f_frag_pos;
smooth out vec3 f_normal;

void main() {
	f_texcoord = texcoord;
	f_normal = normal;
	f_frag_pos = vec3(model*vec4(vertex, 1.0));

	gl_Position = model_view_projection*vec4(vertex, 1.0);
}

#pragma fragment
#pragma include "common.glsl"

uniform sampler2D diffuse;
uniform sampler2D normal;

uniform vec3 cam_position;

smooth in vec3 f_frag_pos;
smooth in vec3 f_normal;
smooth in vec2 f_texcoord;

out vec4 out_frag;

void main() {
	vec3 light_color = vec3(1.0);
	vec3 light_pos = vec3(0.0, 100.0, 0.0);
	vec3 object_color = vec3(1.0, 1.0, 1.0);
	
	// ambient
	vec3 ambient = 0.1*light_color;

	// diffuse 
	vec3 norm = normalize(f_normal);
	vec3 light_dir = normalize(light_pos - f_frag_pos);
	float diff = max(dot(norm, light_dir), 0.0);
	vec3 diffuse = diff*light_color;

	// specular
	vec3 view_dir = normalize(cam_position - f_frag_pos);
	vec3 reflect_dir = reflect(-light_dir, norm);  
	float spec = pow(max(dot(view_dir, reflect_dir), 0.0), 128);
	vec3 specular = 0.5*spec*light_color;  

	vec3 result = ((ambient + diffuse + specular)*object_color + encode_normal(norm))/2.0;
	
	out_frag = vec4(
                result,
		1.0
	);
}

