# Client Server Simulation #

This project attempts to replicate the demo from Glenn Fieldler's article series on [Networked Physics.](http://gafferongames.com/networked-physics/introduction-to-networked-physics/)

It demonstrates rudimentary client-side prediction, jitter buffer, decoupled sim/rendering... and can simulate packet loss, jitter, and round-trip latency. Communication between client and the server thread is implemented using ENet, and packet data is serialized using a bitpacker for compression.

![](https://bitbucket.org/dseyedm/client-server-sim/downloads/best.png)